#include <iostream>
#include "funciones.h"
using namespace std;

/*
Un prototipo de celular muy básico posee las siguientes 3 funcionalidades:
  - Puede enviar mensajes de texto de hasta 256 caracteres
  - Puede realizar llamadas telefónicas
  - Puede tomar fotos.

Este prototipo posee una batería cuya duración, es un valor entero entre 1 y 100, configurable al momento de iniciar el celular.
El prototipo sólo consume su batería cada vez que el usuario realiza una acción.

Los consumos de batería son los siguientes:
  - Por cada mensaje enviado se consume un 1% sobre el total actual.
  - Por cada llamada realizada, se consume en una cantidad fija determinada por: cantidad de minutos * 0,08.
  - Por cada foto tomada consume 6% sobre el total actual.

Realizar un programa que permita a un usuario simular el comportamiento del prototipo mencionado.
Determinar para un ciclo de batería:
  a. Cuál fue la llamada de mayor duración.
  b. El porcentaje de mensajes que superan los 10 caracteres por sobre el total de mensajes enviados.
*/

// Profesores: este ejercicio no requiere interaccion

int main () {
    // Estado inicial
    int bateria = 100;
    int accion = 0, smsChars = 0, minLlamada = 0, cantFotos = 0;
    int maxLlamada = 0;
    int cantSMS = 0, cantSMSMDD = 0;

    srand(time(0));

    while (bateria > 0) {
        accion = (rand() % 3);

        switch (accion) {
            case 0:
                printf("[INFO] Mandando un SMS");

                // Como minimo uso SMS de 10 caracteres
                // No mando los caracteres a la funcion porque en este caso no me sirve
                smsChars = (rand() % 257);
                if (smsChars == 0) {
                    smsChars = 10;
                }

                printf(" de %d caracteres\n", smsChars);

                enviarSMS(bateria);
                cantSMS += 1;

                if (smsChars > 10) {
                    cantSMSMDD += 1;
                }

                break;
            case 1:
                printf("[INFO] Realizando una llamada");

                // Limite de 30 minutos para poder hacer mas facil la simulacion
                minLlamada = (rand() % 31);
                if (minLlamada == 0) {
                    minLlamada = 1;
                }

                if (minLlamada == 1) {
                    printf(" de %d minuto\n", minLlamada);
                } else {
                    printf(" de %d minutos\n", minLlamada);
                }

                realizarLlamada(bateria, minLlamada);

                // Evaluo los minutos de la llamada que realmente se ocuparon,
                // no siempre hay bateria para completar la llamada

                if (minLlamada > maxLlamada) {
                    maxLlamada = minLlamada;
                }
                
                break;
            case 2:
                printf("[INFO] Tomando");

                // Limite de 2 fotos porque esto consume demasiado
                cantFotos = (rand() % 3);
                if (cantFotos == 0) {
                    cantFotos = 1;
                }

                if (cantFotos == 1) {
                    printf(" %d foto\n", cantFotos);
                } else {
                    printf(" %d fotos\n", cantFotos);
                }

                tomarFotos(bateria, cantFotos);
                break;
        }

        if (bateria > 20) {
            printf("[INFO] Carga de la bateria: %d%%\n", bateria);
        } else if (bateria > 0){
            printf("[WARN] Bateria baja! %d%% disponible\n", bateria);
        } else {
            printf("[WARN] Bateria agotada\n");
        }
    }

    printf("\n[INFO] Ciclo de bateria finalizado\n");
    printf("[INFO] La llamada de mayor duracion tuvo una duracion de %d minutos\n", maxLlamada);
    printf("[INFO] El porcentaje de SMS con mas de 10 caracteres fue: %3.2f%% (%d sobre %d)\n", cantSMSMDD * 100.0 / cantSMS, cantSMSMDD, cantSMS);

    return 0;
}

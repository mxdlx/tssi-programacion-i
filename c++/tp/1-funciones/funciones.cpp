#include <iostream>
#include <string>
using namespace std;

// Helpers
int mayor(int a, int b, int c) {
    int r = 0;

    if (a >= b && a >= c) {
        r = a;
    }

    if (b >= a && b >= c) {
        r = b;
    }

    if (c >= a && c >= b) {
        r = c;
    }

    return r;
}

int menor(int a, int b, int c) {
    int r = 0;

    if (a <= b && a <= c) {
        r = a;
    }

    if (b <= a && b <= c) {
        r = b;
    }

    if (c <= a && c <= b) {
        r = c;
    }

    return r;
}

int medio(int a, int b, int c, int m1, int m2) {
    int suma = a + b + c;
    return suma - m1 - m2;
}

// Funciones principales

string dibujarCuadrado (int largo) {
    string r = "";
    for (int i = 0; i < largo; i++) {
        for (int j = 0; j < largo; j++) {
            r.append("*");
        }
        r.append("\n");
    }
    return r;
}

string dibujarTriangulo (int largo) {
    string r = "";
    for (int i = 0; i < largo; i++) {
        for (int j = 0; j <= i; j++) {
            r.append("*");
        }
        r.append("\n");
    }
    return r;
}

int crapulo (int valor) {
    int suma = 0;

    if (valor >= 0 && valor < 10) {
        return valor;
    }

    suma = valor % 10 + crapulo(valor/10);

    if (suma > 10) {
        return crapulo(suma);
    } else {
        return suma;
    }
}

void enviarSMS(int& bateria) {
    bateria -= 1;
}

void realizarLlamada(int& bateria, int& minutos) {
    int desgaste = 0;
    int posible = 0;

    desgaste = 0.08 * minutos;

    if (desgaste > bateria) {
        posible = bateria / 0.08;
        printf("[WARN] No hay bateria suficiente para completar la llamada");

        if (posible == 1){
            printf(", solo podra hablar durante %d minuto\n", posible);
        } else {
            printf(", solo podra hablar durante %d minutos\n", posible);
        }
        minutos = posible;
        bateria = 0;
    } else {
        bateria -= 0.08 * minutos;
    }
}

void tomarFotos(int& bateria, int cantFotos) {
    int desgaste = 0;
    int posible = 0;

    desgaste = 6 * cantFotos;

    if (desgaste > bateria) {
        if (bateria < 6) {
            printf("[ERROR] No hay bateria suficiente para tomar aunque sea una foto\n");
        } else {
            posible = bateria / 6;
            if (posible == 1) {
                printf("[WARN] Solo sera posible tomar %d foto\n", posible);
                bateria -= 6;
            } else {
                printf("[WARN] Solo sera posible tomar %d fotos\n", posible);
                bateria -= 6 * posible;
            }
        }
    } else {
        bateria -= 6 * cantFotos;
    }
}
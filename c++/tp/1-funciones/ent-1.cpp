#include <iostream>
#include "funciones.h"
using namespace std;

/*
En el siguiente ejercicio vamos a tratar de dibujar algunas figuras geométricas como un triángulo rectángulo y un cuadrado.
Para ello, el usuario deberá ingresar qué desea dibujar “triangulo” o “cuadrado” y a continuación deberá ingresar las dimensiones, a saber:
  - En el caso del cuadrado, sólo deberá ingresar el largo del lado.
  - En el caso del triángulo, deberá ingresar el largo de la base.
*/

// Profesores: este ejercicio es interactivo

int main() {

    string figura = "";
    string r = "";
    int largo = 0;

    while (figura != "triangulo" && figura != "cuadrado") {
        cout << "Ingrese la figura a dibujar [triangulo|cuadrado]: ";
        cin >> figura;
    }

    while (largo < 1) {
        cout << "Ingrese el largo de la figura: ";
        cin >> largo;
    }

    if (figura == "cuadrado") {
        r = dibujarCuadrado(largo);
    } else {
        r = dibujarTriangulo(largo);
    }

    printf("\n%s", r.c_str());

    return 0;
}
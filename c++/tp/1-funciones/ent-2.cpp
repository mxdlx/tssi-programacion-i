#include <iostream>
#include "funciones.h"
using namespace std;

/*
Dado un número entero positivo, su crápulo es un número que se obtiene sumando los dígitos que lo componen,
si el valor de la suma es menor que 10. Si la suma es mayor que 10, el crápulo es el crápulo de la suma de los dígitos.
Ejemplo:
  Número Suma Crápula
  7      7    7
  13     4    4
  492    15   6

Se pide, dados trés números:
  - Realizar una función que calcule su crápulo.
  - Realizar una función que permita Imprimir los crápulos de menor a mayor
*/

int main() {
    int a = 0, b = 0, c = 0;

    printf("Ingrese el primer valor: ");
    scanf("%d", &a);

    printf("Ingrese el segundo valor: ");
    scanf("%d", &b);

    printf("Ingrese el tercer valor: ");
    scanf("%d", &c);

    printf("El crapulo de %d es %d\n", a, crapulo(a));
    printf("El crapulo de %d es %d\n", b, crapulo(b));
    printf("El crapulo de %d es %d\n", c, crapulo(c));

    int may = mayor(crapulo(a), crapulo(b), crapulo(c));
    int men = menor(crapulo(a), crapulo(b), crapulo(c));
    int med = medio(crapulo(a), crapulo(b), crapulo(c), may, men);

    printf("El crapulo mayor es %d, el medio %d y el menor %d\n", may, med, men);

    return 0;
}

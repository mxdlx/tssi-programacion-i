#include <iostream>
using namespace std;
#define MAX_HAB_POR_TIPO 20
#define MAX_HABITACIONES 60

// Falso composite
struct Habitacion {
    int numero;
    char estado;
    int okupas;
};

struct HabitacionDoble : Habitacion {
    HabitacionDoble () {
        okupas = 2;
    }
};

struct HabitacionCuad : Habitacion {
    HabitacionCuad () {
        okupas = 4;
    }
};

struct HabitacionOcto : Habitacion {
    HabitacionOcto () {
        okupas = 8;
    }
};

struct Hotel {
    string nombre;
    int categoria;
    Habitacion habitaciones[MAX_HABITACIONES];
};

struct Destino {
    string nombre;
    string descripcion;
    string pais;
    char temporada;
    Hotel hotel;
};
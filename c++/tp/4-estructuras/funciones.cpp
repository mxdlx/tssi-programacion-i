#include <algorithm>
#include <iostream>
#include <random>
#include <string>
#include "estructuras.h"
#define MAX_HABITACIONES 60
#define MAX_HOTELES 30
#define TOP 3
using namespace std;

const string ciudadPais[30][2] = {
        {"Hong Kong", "Hong Kong"}, {"Londres", "Inglaterra"}, {"New York", "Estados Unidos"}, 
        {"Tel Aviv", "Israel"}, {"Tokio", "Japon"}, {"Paris", "Francia"}, 
        {"Singapur", "Singapur"}, {"Viena", "Austria"}, {"Genova", "Italia"}, 
        {"Shangai", "China"}, {"Toronto", "Canada"}, {"Mumbai", "India"}, 
        {"Sidney", "Australia"}, {"Taipei", "Taiwan"}, {"Oslo", "Noruega"},
        {"Estocolmo", "Suecia"}, {"Amsterdam", "Paises Bajos"}, {"Praga", "Republica Checa"}, 
        {"Helsinki", "Finlandia"}, {"Roma", "Italia"}, {"Moscu", "Rusia"}, 
        {"Berlin", "Alemania"}, {"Bermudas", "Bermudas"}, {"Tortola", "Islas Virgenes"}, 
        {"Madrid", "Espania"}, {"Dubai", "Emiratos Arabes"}, {"Luxemburgo", "Luxemburgo"}, 
        {"Estanbul", "Turquia"}, {"Valleta", "Malta"}, {"Copenhagen", "Dinamarca"}
    };

const string descripciones[5] = {
        "La mejor ciudad del mundo", 
        "Un destino que nunca deja de soprender", 
        "La Pepsi de los destinos", 
        "Bueno, bonito y barato",
        "Cuna de la vida nocturna"
    };

const string nombresHotel[5] = {
    "Grand Station", "Hotel Central", "Hotel Comunista", "Gran Hotel", "La Perla"
};

// Testeando como hacer random integers con C++ "moderno"
// RTFM: https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
int numeroRandom (int inicio, int limite) {
    random_device rd;
    mt19937 generator(rd());
    uniform_int_distribution<int> distribution(inicio, limite);

    return distribution(generator);
}

string nombreRandom () {
    return nombresHotel[numeroRandom(0, 4)];
}

char temporadaRandom () {
    int i = numeroRandom(0, 1);
    return i ? 'I' : 'V';
}

void cargarHabitaciones (Habitacion todas[MAX_HABITACIONES]) {
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (i < 20) {
            todas[i] = HabitacionDoble();
        } else if (i < 40) {
            todas[i] = HabitacionCuad();
        } else {
            todas[i] = HabitacionOcto();
        }
        todas[i].estado = 'D';
        todas[i].numero = i;
    }
}

void cargarHoteles (Hotel hoteles[MAX_HOTELES]) {
    for (int i = 0; i < MAX_HOTELES; i++) {
        hoteles[i].categoria = numeroRandom(1, 5);
        hoteles[i].nombre = nombreRandom();
        
        cargarHabitaciones(hoteles[i].habitaciones);
    }
}

void cargarDestinos (Destino destinos[MAX_HOTELES]) {

    Hotel hoteles[MAX_HOTELES];
    cargarHoteles(hoteles);

    for (int i = 0; i < MAX_HOTELES; i++) {
        destinos[i].hotel = hoteles[i];
        destinos[i].nombre = ciudadPais[i][0];
        destinos[i].pais = ciudadPais[i][1];
        destinos[i].descripcion = descripciones[numeroRandom(0, 4)];
        destinos[i].temporada = temporadaRandom(); 
    }

}

void ordenarDestinos (Destino destinos[MAX_HOTELES]) {
    int i, j;
    Destino aux;

    i = 0;
    bool ordenado = false;

    while (i < MAX_HOTELES && !ordenado) {
        ordenado = true;

        for (j = 0; j < MAX_HOTELES - i - 1; j++) {
            if (destinos[j].nombre > destinos[j + 1].nombre) {
                aux = destinos[j];
                destinos[j] = destinos[j + 1];
                destinos[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

int buscarDestino (string destino, Destino destinos[MAX_HOTELES]) {
    int inicio = 0;
    int final = MAX_HOTELES - 1;

    while (final >= inicio) {
        int mitad = inicio + (final - inicio) / 2;

        if (destinos[mitad].nombre == destino) {
            return mitad;
        } 

        if (destinos[mitad].nombre > destino) {
            final = mitad - 1;
        } else {
            inicio = mitad + 1;
        }
    } 

    return -1;
}

int buscarHabitacionDisponible (Habitacion habitaciones[MAX_HABITACIONES], int personas) {
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (habitaciones[i].estado == 'D' && habitaciones[i].okupas >= personas) {
            return i;
        }
    }
    return -1;
}

// Aca solo podria pasar las habitaciones pero la ocupacion _es del_ hotel
int ocupacion(Hotel &h) {
    int oc = 0;
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (h.habitaciones[i].estado == 'O') {
            oc += 1;
        }
    }
    return oc;
}

void topDestinosTemporada (Destino destinos[MAX_HOTELES], char temporada, Destino r[TOP]) {
    for (int i = 0; i < MAX_HOTELES; i++) {
            int j = 0;
            int pos = 0;
        if (destinos[i].temporada == temporada) {
            while (j < TOP) {
                // Si el destino en r esta sin inicializar tiene el nombre vacio
                // Es como hacer un if obj is None en Python pero no
                if (r[j].nombre == "") {
                    r[j] = destinos[i];
                    // Saliendo del while
                    j = TOP - 1;
                } else {
                    int ocCurrent = ocupacion(r[j].hotel);
                    int ocDestino = ocupacion(destinos[i].hotel);

                    if (ocDestino >= ocCurrent) {
                        pos += 1;
                    }
                }
                j++;
            }

            if (pos > 0) {
                for (int j = 0; j < (pos - 1); j++) {
                    r[j] = r[j + 1]; 
                }
                r[pos - 1] = destinos[i];
            }
        }
    }
}
#include "estructuras.h"
#define MAX_HABITACIONES 60
#define MAX_HOTELES 30

void cargarHabitaciones (Habitacion todas[MAX_HABITACIONES]);
void cargarHoteles (Hotel hoteles[MAX_HOTELES]);
void cargarDestinos (Destino destinos[MAX_HOTELES]);
void ordenarDestinos (Destino destinos[MAX_HOTELES]);
int buscarDestino (string destino, Destino destinos[MAX_HOTELES]);
int buscarHabitacionDisponible (Habitacion habitaciones[MAX_HABITACIONES], int personas);
char estadoHabitacion (Habitacion h);
int ocupacion(Hotel &h);
void topDestinosTemporada (Destino destinos[MAX_HOTELES], char temporada, Destino r[3]);
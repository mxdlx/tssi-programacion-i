#include <algorithm>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#define MAX_HAB_POR_TIPO 20
#define MAX_HABITACIONES 60
#define MAX_HOTELES 30
#define TOP 3
using namespace std;

// estructuras.h
struct Habitacion {
    int numero;
    char estado;
    int okupas;
};

struct HabitacionDoble : Habitacion {
    HabitacionDoble () {
        okupas = 2;
    }
};

struct HabitacionCuad : Habitacion {
    HabitacionCuad () {
        okupas = 4;
    }
};

struct HabitacionOcto : Habitacion {
    HabitacionOcto () {
        okupas = 8;
    }
};

struct Hotel {
    string nombre;
    int categoria;
    Habitacion habitaciones[MAX_HABITACIONES];
};

struct Destino {
    string nombre;
    string descripcion;
    string pais;
    char temporada;
    Hotel hotel;
};

// funciones.h funciones.cpp
const string ciudadPais[30][2] = {
        {"Hong Kong", "Hong Kong"}, {"Londres", "Inglaterra"}, {"New York", "Estados Unidos"}, 
        {"Tel Aviv", "Israel"}, {"Tokio", "Japon"}, {"Paris", "Francia"}, 
        {"Singapur", "Singapur"}, {"Viena", "Austria"}, {"Genova", "Italia"}, 
        {"Shangai", "China"}, {"Toronto", "Canada"}, {"Mumbai", "India"}, 
        {"Sidney", "Australia"}, {"Taipei", "Taiwan"}, {"Oslo", "Noruega"},
        {"Estocolmo", "Suecia"}, {"Amsterdam", "Paises Bajos"}, {"Praga", "Republica Checa"}, 
        {"Helsinki", "Finlandia"}, {"Roma", "Italia"}, {"Moscu", "Rusia"}, 
        {"Berlin", "Alemania"}, {"Bermudas", "Bermudas"}, {"Tortola", "Islas Virgenes"}, 
        {"Madrid", "Espania"}, {"Dubai", "Emiratos Arabes"}, {"Luxemburgo", "Luxemburgo"}, 
        {"Estanbul", "Turquia"}, {"Valleta", "Malta"}, {"Copenhagen", "Dinamarca"}
    };

const string descripciones[5] = {
        "La mejor ciudad del mundo", 
        "Un destino que nunca deja de soprender", 
        "La Pepsi de los destinos", 
        "Bueno, bonito y barato",
        "Cuna de la vida nocturna"
    };

const string nombresHotel[5] = {
    "Grand Station", "Hotel Central", "Hotel Comunista", "Gran Hotel", "La Perla"
};

// Testeando como hacer random integers con C++ "moderno"
// RTFM: https://diego.assencio.com/?index=6890b8c50169ef45b74db135063c227c
int numeroRandom (int inicio, int limite) {
    random_device rd;
    mt19937 generator(rd());
    uniform_int_distribution<int> distribution(inicio, limite);

    return distribution(generator);
}

string nombreRandom () {
    return nombresHotel[numeroRandom(0, 4)];
}

char temporadaRandom () {
    int i = numeroRandom(0, 1);
    return i ? 'I' : 'V';
}

void cargarHabitaciones (Habitacion todas[MAX_HABITACIONES]) {
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (i < 20) {
            todas[i] = HabitacionDoble();
        } else if (i < 40) {
            todas[i] = HabitacionCuad();
        } else {
            todas[i] = HabitacionOcto();
        }
        todas[i].estado = 'D';
        todas[i].numero = i;
    }
}

void cargarHoteles (Hotel hoteles[MAX_HOTELES]) {
    for (int i = 0; i < MAX_HOTELES; i++) {
        hoteles[i].categoria = numeroRandom(1, 5);
        hoteles[i].nombre = nombreRandom();
        
        cargarHabitaciones(hoteles[i].habitaciones);
    }
}

void cargarDestinos (Destino destinos[MAX_HOTELES]) {

    Hotel hoteles[MAX_HOTELES];
    cargarHoteles(hoteles);

    for (int i = 0; i < MAX_HOTELES; i++) {
        destinos[i].hotel = hoteles[i];
        destinos[i].nombre = ciudadPais[i][0];
        destinos[i].pais = ciudadPais[i][1];
        destinos[i].descripcion = descripciones[numeroRandom(0, 4)];
        destinos[i].temporada = temporadaRandom(); 
    }

}

void ordenarDestinos (Destino destinos[MAX_HOTELES]) {
    int i, j;
    Destino aux;

    i = 0;
    bool ordenado = false;

    while (i < MAX_HOTELES && !ordenado) {
        ordenado = true;

        for (j = 0; j < MAX_HOTELES - i - 1; j++) {
            if (destinos[j].nombre > destinos[j + 1].nombre) {
                aux = destinos[j];
                destinos[j] = destinos[j + 1];
                destinos[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

int buscarDestino (string destino, Destino destinos[MAX_HOTELES]) {
    int inicio = 0;
    int final = MAX_HOTELES - 1;

    while (final >= inicio) {
        int mitad = inicio + (final - inicio) / 2;

        if (destinos[mitad].nombre == destino) {
            return mitad;
        } 

        if (destinos[mitad].nombre > destino) {
            final = mitad - 1;
        } else {
            inicio = mitad + 1;
        }
    } 

    return -1;
}

int buscarHabitacionDisponible (Habitacion habitaciones[MAX_HABITACIONES], int personas) {
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (habitaciones[i].estado == 'D' && habitaciones[i].okupas >= personas) {
            return i;
        }
    }
    return -1;
}

// Aca solo podria pasar las habitaciones pero la ocupacion _es del_ hotel
int ocupacion(Hotel &h) {
    int oc = 0;
    for (int i = 0; i < MAX_HABITACIONES; i++) {
        if (h.habitaciones[i].estado == 'O') {
            oc += 1;
        }
    }
    return oc;
}

void topDestinosTemporada (Destino destinos[MAX_HOTELES], char temporada, Destino r[TOP]) {
    for (int i = 0; i < MAX_HOTELES; i++) {
            int j = 0;
            int pos = 0;
        if (destinos[i].temporada == temporada) {
            while (j < TOP) {
                // Si el destino en r esta sin inicializar tiene el nombre vacio
                // Es como hacer un if obj is None en Python pero no
                if (r[j].nombre == "") {
                    r[j] = destinos[i];
                    // Saliendo del while
                    j = TOP - 1;
                } else {
                    int ocCurrent = ocupacion(r[j].hotel);
                    int ocDestino = ocupacion(destinos[i].hotel);

                    if (ocDestino >= ocCurrent) {
                        pos += 1;
                    }
                }
                j++;
            }

            if (pos > 0) {
                for (int j = 0; j < (pos - 1); j++) {
                    r[j] = r[j + 1]; 
                }
                r[pos - 1] = destinos[i];
            }
        }
    }
}

// UI
// Basicos
void uiMotd () {
    printf("##### Bienvenido a Despegar Dial Up #####\n");
    printf(">> Presione Ctrl-C para finalizar el proceso.\n");
}

void uiMenu (int &s) {
    s = -1;
    while (s < 0 || s > 3) {
        printf("\nSeleccione la accion a realizar: ");
        printf("\n--------------------------------\n");
        printf("-- 1: Realizar reserva.\n");
        printf("-- 2: Liberar habitacion.\n");
        printf("-- 3: Listar destinos con mayor ocupacion segun temporada.\n");
        cout << "Opcion: ";
        cin >> s;

        if (cin.fail() || s < 0 || s > 3) {
            // Que dificil es sanitizar input en este lenguaje
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n");
        }
    }
}

// UI ingreso de datos
void uiPersonas (int &personas) {
    personas = -1;
    while (personas < 0 || personas > 8) {
        printf("\nCantidad de personas");
        printf("\n--------------------\n");
        printf(">> Ingrese la cantidad de personas para la reserva: ");
        scanf("%d", &personas);

        if (cin.fail() || personas < 0 || personas > 8) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("El numero de personas es superior al limite o el valor ingresado no es valido!\n");
        }
    }
}

void uiHabitacion (int &nroHabitacion) {
    nroHabitacion = -1;

    while (nroHabitacion < 0 || nroHabitacion > 60) {
        printf("\nNumero de habitacion reservada");
        printf("\n------------------------------\n");
        printf(">> Ingrese el numero de habitacion: ");
        scanf("%d", &nroHabitacion);

        if (cin.fail() || nroHabitacion < 0 || nroHabitacion > 60) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("El numero de habitacion es superior al limite o el valor ingresado no es valido!\n");
        }
    }
}

// UI comun
int uiDestino (Destino destinos[]) {
    string destino;
    // Tengo destinos con espacios asi que tenia que usar getline.
    // Por lo que entendi tenia que consumir el new line del menu principal con cin.get()
    cin.get();
    printf("\nBusqueda de destino");
    printf("\n-------------------\n");
    printf(">> Ingrese el destino del cliente: ");
    getline(cin, destino);
        
    int d = buscarDestino(destino, destinos);
    if (d != -1) {
        printf(">> Destino encontrado!\n");
        printf(">> El destino es %s (Temporada %c)\n", destinos[d].nombre.c_str(), destinos[d].temporada);
    } else {
        printf(">> Destino no encontrado!\n");
    }
    return d;
}

// UI Reserva
int uiHabitacionLibre (Habitacion habitaciones[], int personas) {
    int h = buscarHabitacionDisponible(habitaciones, personas);
    if (h != -1) {
        printf(">> Habitacion encontrada!\n");
    } else {
        printf(">> No hay habitaciones disponibles!\n");
    }

    return h;
}

int uiReservarHabitacion (Habitacion &h) {
    printf("\nReserva");
    printf("\n-------\n");
    
    char c = 'X';
    int r = -1;

    while (c != 's' && c != 'n'){
        printf(">> Reservar la habitacion? (s/n): ");
        cin >> c;

        if (cin.fail() || c != 's' && c != 'n') {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (h.estado != 'O') {
                if (c == 's') {
                    h.estado = 'O';
                    printf("Reserva realizada!\n");
                    r = 0;
                } else if (c == 'n') {
                    printf("Reserva cancelada!\n");
                } else {
                    printf("Respuesta no valida!\n");
                }
            } else {
                printf("Error! La habitacion solicitada se encuentra ocupada!\n");
            }
        }
    }
    return r;
}

void uiFinalReserva (string hotel, string ciudad, int habitacion, int capacidad, int personas) {
    printf("\nDatos de la reserva ");
    printf("\n-------------------\n");
    printf(">> Reserva realizada en el hotel %s en la ciudad de %s\n", hotel.c_str(), ciudad.c_str());
    printf(">> Reservada la habitacion Numero %d (Capacidad %d personas)\n", habitacion + 1, capacidad);
    printf(">> Cantidad de personas: %d\n", personas);
}

// UI Liberar habitacion
int uiHabitacionOcupada (Habitacion habitaciones[], int h) {
    int r = -1;
    printf("\nBusqueda de reserva");
    printf("\n-------------------\n");

    if (habitaciones[h].estado == 'O') {
        // Asumimos que esta es la reserva
        printf(">> Reserva encontrada!\n");
        r = 0;
    } else {
        printf("Error! La habitacion indicada se encuentra libre!\n");
    }
    return r;
}

int uiLiberarHabitacion (Habitacion &h) {
    printf("\nLiberar habitacion");
    printf("\n------------------\n");

    char c = 'X';
    int r = -1;

    while (c != 's' && c != 'n'){
        printf(">> Liberar la habitacion? (s/n): ");
        cin >> c;

        if (cin.fail() || c != 's' && c != 'n') {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (c == 's') {
                h.estado = 'D';
                printf("Habitacion liberada!\n");
                r = 0;
            } else if (c == 'n') {
                printf("Accion cancelada!\n");
            } else {
                printf("Respuesta no valida!\n");
            }
        }
    }
    return r;
}

void uiFinalLiberacion (string hotel, string ciudad, int habitacion) {
    printf("\nDatos de la liberacion");
    printf("\n----------------------\n");
    printf(">> Reserva liberada en el hotel %s en la ciudad de %s\n", hotel.c_str(), ciudad.c_str());
    printf(">> Liberada la habitacion Numero %d\n", habitacion + 1);
}

void uiTopDestinos(Destino destinos[]) {
    printf("\nTop %d de destinos por Temporada", TOP);
    printf("\n-------------------------------\n");

    string temporada;
    char t;
    Destino d[TOP];

    // Restrinjo los valores segun el enunciado
    while (temporada != "Invierno" && temporada != "Verano"){
        printf(">> Ingrese la temporada: ");
        cin >> temporada;

        if (cin.fail() || temporada != "Invierno" && temporada != "Verano") {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            printf("Opcion invalida!\n"); 
        } else {
            if (temporada == "Invierno") {
                t = 'I';
            } else {
                t = 'V';
            }
            topDestinosTemporada(destinos, t, d);

            int ranking = TOP;
            for (int i = 0; i < TOP; i++) {
                int oc = ocupacion(d[i].hotel);
                printf("El destino numero %d", ranking); 
                if (oc == 1) {
                    printf(" es %s con %d habitacion ocupada\n", d[i].nombre.c_str(), oc);
                } else {
                    printf(" es %s con %d habitaciones ocupadas\n", d[i].nombre.c_str(), oc);
                }
                ranking--;
            }
        }
    } 
}

// Main aka Experto
int main () {
    // Setup inicial
    Destino destinos[MAX_HOTELES];
    cargarDestinos(destinos);
    ordenarDestinos(destinos);

    uiMotd();

    while (true) {
        int s;
        uiMenu(s);

        switch (s) {
            case 1: {
                // Busqueda del destino
                int d = uiDestino(destinos);
                if (d != -1) {
                    // Busqueda de habitacion
                    int personas;
                    uiPersonas(personas);
                    int h;
                    if (d != -1) {
                        h = uiHabitacionLibre(destinos[d].hotel.habitaciones, personas);
                    }        
                    // Reserva de habitacion
                    int r;
                    if (h != -1) {
                        r = uiReservarHabitacion(destinos[d].hotel.habitaciones[h]);
                    }
                    // Mensaje final
                    if (r != -1) {
                        uiFinalReserva(destinos[d].hotel.nombre, destinos[d].nombre, h, destinos[d].hotel.habitaciones[h].okupas, personas);
                    }
                }
                break;
            }
            case 2: {
                // Busqueda del destino
                int d = uiDestino(destinos);
                if (d != -1) {
                    // Busqueda de habitacion
                    int h;
                    uiHabitacion(h);
                    int hr = h - 1;
                    int r = uiHabitacionOcupada(destinos[d].hotel.habitaciones, hr);
                    // Operation freedom
                    int l;
                    if (r != -1) {
                        l = uiLiberarHabitacion(destinos[d].hotel.habitaciones[hr]);
                    }
                    // Mensaje Final
                    if (l != -1) {
                        uiFinalLiberacion(destinos[d].hotel.nombre, destinos[d].nombre, hr);
                    }
                }
                break;
            }
            case 3: {
                uiTopDestinos(destinos);
                break;
            }
        }
    }
}
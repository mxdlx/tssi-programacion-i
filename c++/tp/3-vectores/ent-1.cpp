#include <iostream>
using namespace std;

void obtenerSemana(float mes[], float r[], int semana) {
    int bordeDerecho = semana * 7;
    int bordeIzquierdo = bordeDerecho - 7;
    int indice = 0;

    for (int i = bordeIzquierdo; i < bordeDerecho; i++) {
        r[indice] = mes[i];
        indice++;
    }
}

string tendencia(float semana[]) {
    string t = "";
    int contCreciente = 0;
    int contDecreciente = 0;

    for (int i = 0; i < 6; i++) {
        if (semana[i] <= semana[i + 1]) {
            contCreciente += 1;
        }

        if (semana[i] >= semana[i + 1]) {
            contDecreciente += 1;
        }
    }

    if (contCreciente == 6) {
        t = "creciente";
    } else if (contDecreciente == 6) {
        t = "decreciente";
    } else {
        t = "otra";
    }   

    return t;
}

void maximaMensual(float mes[], float& maxima, int& semana) {
    float max = 0;
    int dia = 0;

    for (int i = 0; i < 28; i++) {
        if (mes[i] >= max) {
            max = mes[i];
            dia = i;
        }
    }

    maxima = max;

    if (dia < 7) {
        semana = 1;
    } else if (dia < 14) {
        semana = 2;
    } else if (dia < 21) {
        semana = 3;
    } else {
        semana = 4;
    }
}

int obtenerDia(string dia) {
    int r = 0;

    if (dia == "Martes" || dia == "martes") {
        r = 1;
    }
    if (dia == "Miercoles" || dia == "miercoles") {
        r = 2;
    }
    if (dia == "Jueves" || dia == "jueves") {
        r = 3;
    }
    if (dia == "Viernes" || dia == "viernes") {
        r = 4;
    }
    if (dia == "Sabado" || dia == "sabado") {
        r = 5;
    }
    if (dia == "Domingo" || dia == "domingo") {
        r = 6;
    }

    return r;
}

void parteDia (float mes[], string dia, float r[]) {
    int numeroDia = obtenerDia(dia);
    int puntero = 0;

    for (int i = numeroDia; i < 28; i += 7) {
        r[puntero] = mes[i];
        puntero++;
    }
}

int main () {

    float mes[28];

    for (int i = 0; i < 28; i++) {
        printf("Ingrese una temperatura: ");
        scanf("%f", &mes[i]);
    }

    printf("\nReporte de tendencias semanales\n");
    printf("-------------------------------\n");
    float semana[7];
    for (int i = 0; i < 4; i++) {
        obtenerSemana(mes, semana, i + 1);

        string t = tendencia(semana);
        printf("Semana %d: %s\n", i + 1, t.c_str());
    }

    printf("\nReporte de maxima mensual\n");
    printf("-------------------------\n");
    float maxima = 0;
    int sem = 0;
    maximaMensual(mes, maxima, sem);
    printf("La maxima del mes fue de %4.2f ºC durante la semana %d\n", maxima, sem);

    printf("\nReporte de temperatura: mismo dia del mes\n");
    printf("-----------------------------------------\n");
    string dia = "";
    cout << "Ingrese el dia de la semana que quiere inspeccionar: ";
    cin >> dia;
    float r[4];
    parteDia(mes, dia, r);

    printf("Reporte para el dia %s\n", dia.c_str());
    for (int i = 0; i < 4; i++) {
        printf("Semana %d: %4.2f ºC\n", i + 1, r[i]);
    }

    return 0;
}
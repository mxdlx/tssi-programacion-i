#include <iostream>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

float fixture[28] = {20, 25.2, 28.5, 28.5, 30.6, 30.7, 31, 30, 29.9, 29, 25, 24.2, 23.1, 21.9, 23.4, 18.3, 25.2, 19, 24.1, 20.7, 20.3, 18.1, 19.1, 20.1, 21.1, 22.1, 17.1, 22};
//                   | semana 1                            | semana 2                          | semana 3                              | semana 4

// Helpers
bool checkArray (float first[], float segundo[], float cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        //printf("%f - %f\n", first[i], segundo[i]);
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

TEST_CASE ( "Extraer la primera semana ") {
    float esperado[] = {20, 25.2, 28.5, 28.5, 30.6, 30.7, 31};
    float r[7];

    obtenerSemana(fixture, r, 1);

    bool equalArrays = checkArray(esperado, r, 7);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Extraer la segunda semana ") {
    float esperado[] = {30, 29.9, 29, 25, 24.2, 23.1, 21.9};
    float r[7];

    obtenerSemana(fixture, r, 2);

    bool equalArrays = checkArray(esperado, r, 7);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Extraer la tercera semana ") {
    float esperado[] = {23.4, 18.3, 25.2, 19, 24.1, 20.7, 20.3};
    float r[7];

    obtenerSemana(fixture, r, 3);

    bool equalArrays = checkArray(esperado, r, 7);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Extraer la cuarta semana ") {
    float esperado[] = {18.1, 19.1, 20.1, 21.1, 22.1, 17.1, 22};
    float r[7];

    obtenerSemana(fixture, r, 4);

    bool equalArrays = checkArray(esperado, r, 7);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Tendencia creciente" ) {
    float semana[7];
    obtenerSemana(fixture, semana, 1);
    string esperado = "creciente";

    string r = tendencia(semana);

    CHECK_THAT(r, Catch::Equals(esperado));
}

TEST_CASE ( "Tendencia decreciente" ) {
    float semana[7];
    obtenerSemana(fixture, semana, 2);
    string esperado = "decreciente";

    string r = tendencia(semana);

    CHECK_THAT(r, Catch::Equals(esperado));
}

TEST_CASE ( "Tendencia otra" ) {
    float semana[7];
    obtenerSemana(fixture, semana, 3);
    string esperado = "otra";

    string r = tendencia(semana);

    CHECK_THAT(r, Catch::Equals(esperado));
}

TEST_CASE ( "Maxima del mes" ) {
    int semana = 0;
    float maxMensual = 0.0;

    maximaMensual(fixture, maxMensual, semana);

    REQUIRE( maxMensual == 31);
    REQUIRE( semana == 1);
}

TEST_CASE ( "Obtener valor numerico de dia lunes" ) {
    string dia = "lunes";
    int esperado = 0;
    int r = obtenerDia(dia);

    REQUIRE (r == esperado);
}

TEST_CASE ( "Obtener valor numerico de dia sabado" ) {
    string dia = "sabado";
    int esperado = 5;
    int r = obtenerDia(dia);

    REQUIRE (r == esperado);
}

TEST_CASE ( "Parte de temperatura mismo dia cada semana - Lunes" ) {
    string dia = "lunes";
    float r[4];
    float esperado[4] = {20, 30, 23.4, 18.1};

    parteDia(fixture, dia, r);

    bool equalArrays = checkArray(esperado, r, 4);

    REQUIRE ( equalArrays );
}

TEST_CASE ( "Parte de temperatura mismo dia cada semana - Sabado" ) {
    string dia = "sabado";
    float r[4];
    float esperado[4] = {30.7, 23.1, 20.7, 17.1};

    parteDia(fixture, dia, r);

    bool equalArrays = checkArray(esperado, r, 4);

    REQUIRE ( equalArrays );
}
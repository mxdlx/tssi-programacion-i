using namespace std;

void obtenerSemana(float mes[], float r[], int semana);
string tendencia(float semana[]);
void maximaMensual(float mes[], float& maxima, int& semana);
int obtenerDia(string dia);
void parteDia (float mes[], string dia, float r[]);
#include <iostream>
using namespace std;

/*
Una Empresa dedicada a realizar servicios para fiestas, nos pide la confección de un programa
que les permita obtener algunos datos estadísticos procesando la información que tienen
de las fiestas que se van a realizar. Para ello ingresan la siguiente información ya validada:
  - Fecha de la fiesta (8 dígitos en formato AAAAMMDD)
  - Tipo de fiesta (‘C’, ’S', ‘O’)
  - Cantidad de personas
  - Nº de menú elegido (1, ó 2). En cada fiesta sólo se elige un menú.
El proceso finaliza cuando se ingresa una fecha negativa. Se pide:
  - a. Informar cuantas fiestas hay de cada tipo.
  - b. La fecha de la fiesta con mayor cantidad de personas (único máximo)
  - c. La fecha de la fiesta con mayor cantidad de menú 1 y la fecha de la fiesta con mayor cantidad de menú 2. (suponer único
máximo)
  - d. El promedio de personas de todas las fiestas.
Nota: Para el punto “b” y “c” las fechas deben ser impresas como DD/MM/YYYY */

/* ATENCION: compilar y correr este codigo en macOS Catalina puede devolver valores erroneos */

void prettyFecha (int fecha) {
  int anio = fecha / 10000;
  int mes = (fecha - anio * 10000) / 100;
  int dia = fecha - anio * 10000 - mes * 100;

  printf("%02d/%02d/%d", dia, mes, anio);
}

char getTipoFiesta () {
    char tipoFiesta;
    tipoFiesta = 'X';

    while (tipoFiesta != 'C' && tipoFiesta != 'S' && tipoFiesta != 'O') {
        printf("Ingrese el tipo de fiesta: ");
        scanf("%s", &tipoFiesta);

        if (tipoFiesta != 'C' && tipoFiesta != 'S' && tipoFiesta != 'O') {
            printf("[ERROR] El tipo de fiesta es incorrecto!\n");
        }
    }

    return tipoFiesta;
}

int getCantidadGente () {
  int cantidadGente = 0;

  while (cantidadGente < 1) {
    printf("Ingrese la cantidad de asistentes: ");
    scanf("%d", &cantidadGente);

    if (cantidadGente < 1) {
      printf("[ERROR] La cantidad de asistentes debe ser por lo menos 1!\n");
    }
  }

  return cantidadGente;
}

int getMenuElegido () {
  int menu = 0;

  srand(time(0));

  menu = (rand() % 3);

  if (menu == 0) {
    menu = 1;
  }

  printf("[INFO] Menu elegido: %d \n", menu);
  return menu;
}

void informarCantidadFiestas (char tipo, int cantidad) {
  printf("[INFO] La cantidad de fiestas %c fue %d\n", tipo, cantidad);
}

void setMaximo(int& maximo, int& valor, int& fechaMax, int& fecha) {
  if (valor > maximo) {
    maximo = valor;
    fechaMax = fecha;
  }
}

void informarMaximo(string mensaje, int fecha) {
  // std::format recien existe desde Febrero 2020
  // https://en.cppreference.com/w/cpp/utility/format/format
  // Esta funcion es un _best effort_
  printf("La fiesta con %s fue en ", mensaje.c_str());
  prettyFecha(fecha);
  printf("\n");
}

int main() {
  // Vars
  char tipoFiesta; tipoFiesta = 'X';
  int cantidadGente = 0, menu = 0;

  // Contadores
  int fiestasC = 0, fiestasS = 0, fiestasO = 0, totalAsistentes = 0;
  int maxAsistencia = 0, maxAsistenciaM1 = 0, maxAsistenciaM2 = 0;
  int fechaMaxAsistencia = 0, fechaMaxAsistenciaM1 = 0, fechaMaxAsistenciaM2 = 0;

  // Helpers
  bool data = false;

  printf(">> Sistema de Servicio de Fiestas\n");

  while (true) {
    int fecha = 0;

    printf("Ingrese la fecha de la fiesta: ");
    scanf("%d", &fecha);

    if (fecha < 0) {
      break;
    }

    tipoFiesta = getTipoFiesta();
    cantidadGente = getCantidadGente();
    menu = getMenuElegido();

    // No tengo un tipo de dato como para no repetir este codigo
    if (tipoFiesta == 'C') {
      fiestasC += 1;
    }

    if (tipoFiesta == 'S') {
      fiestasS += 1;
    }

    if (tipoFiesta == 'O') {
      fiestasO += 1;
    }

    setMaximo(maxAsistencia, cantidadGente, fechaMaxAsistencia, fecha);

    if (menu == 1) {
      setMaximo(maxAsistenciaM1, cantidadGente, fechaMaxAsistenciaM1, fecha);
    } else {
      setMaximo(maxAsistenciaM2, cantidadGente, fechaMaxAsistenciaM2, fecha);
    }

    totalAsistentes += cantidadGente;

    data = true;
  }

  printf("\n>> Resultados:\n");

  if (data == true) {
    informarCantidadFiestas('C', fiestasC);
    informarCantidadFiestas('S', fiestasS);
    informarCantidadFiestas('O', fiestasO);
    informarMaximo("mayor numero de asistentes", fechaMaxAsistencia);

    if (fechaMaxAsistenciaM1 > 0) {
      informarMaximo("mayor cantidad de Menu Nº 1", fechaMaxAsistenciaM1);
    }

    if (fechaMaxAsistenciaM2 > 0) {
      informarMaximo("mayor cantidad de Menu Nº 2", fechaMaxAsistenciaM2);
    }

    printf("El promedio de gente por fiesta fue de %4.2f\n", (float)totalAsistentes / (fiestasC + fiestasS + fiestasO));

  } else {
    printf("No se ingresaron fiestas!\n");
  }

  return 0;

}

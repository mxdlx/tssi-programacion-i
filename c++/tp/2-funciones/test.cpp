#include <iostream>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

TEST_CASE ( "Cuenta regresiva" ) {
    int r = cuentaRegresivaPura(5);

    REQUIRE( r == 15 );
}
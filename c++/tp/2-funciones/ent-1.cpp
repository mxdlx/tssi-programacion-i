#include <iostream>
using namespace std;

/*
El centro de atención al cliente de una conocida marca de electrodomésticos
recibe diariamente llamados e ingresa por cada uno los siguientes datos ya validados:
  - Código de comercio donde adquirió el producto (entero entre 1 y 20)
  - Código del producto por el cual llama (entero entre 1 y 300)
  - Tipo de llamado (‘C’ => Consulta, ‘R’ => Reclamo)
  - Duración de la llamada en minutos (mayor que 0 siempre)
Al finalizar la llamada, se registra también la calificación dada por el cliente a la atención
brindada. Esta calificación es un número entre 1 y 3, siendo:
  - 1 muy mala
  - 2 normal
  - 3 excelente
El proceso finaliza cuando se ingresa un código de comercio menor a 1 o mayor a 20.
Se pide procesar toda la información correspondiente a las llamadas e indicar:
  - a. Porcentaje de llamadas por consulta sobre el total de llamadas.
  - b. Porcentaje de llamadas por reclamos sobre el total de llamadas.
  - c. Código del producto correspondiente a la llamada más larga para consulta.
  - d. Promedio de llamadas calificadas como “excelente” sobre el total de llamadas.
  - e. Promedio de llamadas calificadas como “muy mala” sobre el total de llamadas.
*/

/* ATENCION: compilar y correr este codigo en macOS Catalina puede devolver valores erroneos */

int getCodigoProducto () {
    int codigoProducto = 0;

    while (codigoProducto < 1 || codigoProducto > 300) {
        printf("Ingrese el codigo de producto: ");
        scanf("%d", &codigoProducto);

        if (codigoProducto < 1 || codigoProducto > 300) {
            printf("[ERROR] El codigo ingresado es incorrecto!\n");
        }
    }

    return codigoProducto;
}

string getTipoDeLlamada () {
    string tipoLlamada = "";

    while (tipoLlamada != "C" && tipoLlamada != "R") {
	cout << "Ingrese el tipo de llamada recibida: ";
	cin >> tipoLlamada;

        if (tipoLlamada != "C" && tipoLlamada != "R") {
            printf("[ERROR] El tipo de llamada es incorrecto!\n");
        }
    }

    return tipoLlamada;
}

int getDuracionLlamada () {
    int duracionLlamada = 0;

    while (duracionLlamada < 1) {
        printf("Ingrese la duracion de la llamada: ");
        scanf("%d", &duracionLlamada);

        if (duracionLlamada < 1) {
            printf("[ERROR] La llamada debe tener una duracion de por lo menos un minuto!\n");
        }
    }

    return duracionLlamada;
}

int getCalificacionLlamada () {
    int calificacionLlamada = 0;

    while (calificacionLlamada < 1 || calificacionLlamada > 3) {
	printf("Ingrese la calificacion de la llamada: ");
	scanf("%d", &calificacionLlamada);

	if (calificacionLlamada < 1 || calificacionLlamada > 3) {
	    printf("[ERROR] La calificacion de la llamada debe ser 1, 2 o 3!\n");
	}
    }

    return calificacionLlamada;
}

int main() {
    // Vars
    int codigoProducto = 0, duracionLlamada = 0, calificacionLlamada = 0;
    string tipoLlamada = "";

    // Contadores
    int cantLlamadasC = 0, cantLlamadasR = 0, cantLlamadas = 0;
    int cantCalifExc = 0, cantCalifMMala = 0;

    // Helpers
    int productoMaxDuracion = 0, maxDuracionLlamada = 0;

    bool data = false;

    printf("\n>> Sistema de procesamiento de llamadas\n");

    while (true) {
        int codigoComercio = 0;

        printf("Ingrese el codigo del comercio donde se adquirio el producto: ");
        scanf("%d", &codigoComercio);

        if (codigoComercio < 1 || codigoComercio > 20) {
            break;
        }

        codigoProducto = getCodigoProducto();
        tipoLlamada = getTipoDeLlamada();
        duracionLlamada = getDuracionLlamada();

        if (tipoLlamada == "C") {
            cantLlamadasC += 1;

            if (duracionLlamada > maxDuracionLlamada) {
                maxDuracionLlamada = duracionLlamada;
                productoMaxDuracion = codigoProducto;
            }

        } else {
            cantLlamadasR += 1;
        }

        calificacionLlamada = getCalificacionLlamada();

        if (calificacionLlamada == 3) {
            cantCalifExc += 1;
        }

        if (calificacionLlamada == 1) {
            cantCalifMMala += 1;
        }

        cantLlamadas += 1;
        data = true;
    }

    printf("\n>> Resultados:\n");

    if (data == true) {
        printf("[INFO] El porcentaje de llamadas por Consultas fue %2.2f%%\n", cantLlamadasC * 100.0 / cantLlamadas);
        printf("[INFO] El porcentaje de llamadas por Reclamos fue %2.2f%%\n", cantLlamadasR * 100.0 / cantLlamadas);
        printf("[INFO] El codigo de producto correspondiente a la llamada mas larga por Consulta fue %d\n", productoMaxDuracion);
        printf("[INFO] El promedio de llamadas calificadas como Excelente es %2.2f\n", (float)cantCalifExc / (float)cantLlamadas);
        printf("[INFO] El promedio de llamadas calificadas como Muy Mala es %2.2f\n", (float)cantCalifMMala / (float)cantLlamadas);
    } else {
        printf("[WARN] No se ingresaron llamadas\n");
    }

    return 0;
}

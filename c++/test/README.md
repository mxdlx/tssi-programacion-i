# Unit Testing con Catch

Primero es necesario contar con un main.cpp, en este caso es `test-main.cpp` que debe tener el siguiente contenido:

```
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
```

Luego hay que codificar tanto el archivo de funciones, `funciones.cpp` y su header, `funciones.h`. Y compilar:

```
$ g++ test-main.cpp -c
$ g++ funciones.cpp -c
```

Luego en el archivo de tests, `test.cpp`, tener la siguiente cabecera:

```
#include <iostream>
#include "catch.hpp"
#include "funciones.h"
using namespace std;

// tests
```

Y compilar:

```
$ g++ test-main.o funciones.o test.cpp -o test.bin

# Ya podemos correr los tests

$ ./test.bin -s
```
#include <iostream>
#include "catch.hpp"
using namespace std;

int getAnio (int fecha) {
	return fecha / 10000;
}

int getMes (int fecha) {
	int anio = getAnio(fecha);

	return (fecha - anio * 10000) / 100;
}

int getDia (int fecha) {
	int anio = getAnio(fecha);
	int mes = getMes(fecha);

	return (fecha - anio * 10000) - mes * 100;
}

TEST_CASE( "anio" ) {
	REQUIRE( getAnio(19890826) == 1989 );
}

TEST_CASE( "mes-sola-cifra" ) {
	REQUIRE( getMes(19890826) == 8 );
}

TEST_CASE( "mes-dos-cifras" ) {
	REQUIRE( getMes(19891226) == 12 );
}

TEST_CASE( "dia-sola-cifra" ) {
	REQUIRE( getDia(19890801) == 1 );
}

TEST_CASE( "dia-dos-cifras" ) {
	REQUIRE( getDia(19891226) == 26 );
}

#include <iostream>
#include <algorithm>
#define MAX_JUEGOS 100
#define MAX_USUARIOS 1000
using namespace std;

struct Juego {
    int codigo;
    string nombre;
};

struct Usuario {
    int codigo;
    string nombre;
    int fechaRegistro;
};

// Le llamo compra, soy Steam
struct Compra {
    Juego jog;
    Usuario usr;
};

void ordenarComprasPorJuego (Compra c[], int cant) {
    int i = 0;
    int j;
    Compra aux;
    bool ordenado = false;

    while (i < cant && !ordenado) {
        ordenado = true;

        for (j = 0; j < cant - i - 1; j++) {
            if (c[j].jog.codigo > c[j + 1].jog.codigo) {
                aux = c[j];
                c[j] = c[j + 1];
                c[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

// Uso este procedimiento una vez que las compras estan ordenadas por Juego
void ordenarComprasPorUsuario (Compra c[], int cant) {
    int i = 0;
    int j;
    Compra aux;
    bool ordenado = false;

    while (i < cant && !ordenado) {
        ordenado = true;

        for (j = 0; j < cant - i - 1; j++) {
            if (c[j].jog.codigo == c[j+1].jog.codigo && c[j].usr.codigo > c[j + 1].usr.codigo) {
                aux = c[j];
                c[j] = c[j + 1];
                c[j + 1] = aux;
                ordenado = false;
            }
        }
        i++;
    }
}

void listarUsuariosPorJuegos (Compra c[], int cant) {
    int i  = 0;
    int codigoJuego;

    while (i < cant) {
        codigoJuego = c[i].jog.codigo;
        string nombre = c[i].jog.nombre;
        int sumaUsuarios = 0;

        printf(">> Juego: %s\n", nombre.c_str());

        while (i < cant && c[i].jog.codigo == codigoJuego) {
            printf("  >> Usuario: %s - Fecha de Registro: %d\n", c[i].usr.nombre.c_str(), c[i].usr.fechaRegistro);
            sumaUsuarios += 1;
            i++;
        }

        printf("== Cantidad total de usuarios del juego: %d\n", sumaUsuarios);
    }
}

const string nombresJuegos[5] = {
    "TombRaider",
    "MortalKombat",
    "GranTurismo",
    "StreetFighter",
    "Asteroids"
};

const string nombresUsuarios[5] = {
    "Alejandro",
    "Leo",
    "Steve",
    "Bill",
    "Linus"
};

void generarJuegos (Juego j[MAX_JUEGOS], int cant) {
    int posiciones[MAX_JUEGOS];

    for (int i = 0; i < cant; i++) {
        posiciones[i] = i + 100;
    }

    random_shuffle(std::begin(posiciones), std::end(posiciones));

    srand(time(0));

    for (int i = 0; i < cant; i++) {
        int n = rand() % 5;

        j[i].codigo = posiciones[i];
        j[i].nombre = nombresJuegos[n].c_str() + std::to_string(i); 
    }
}

void generarUsuarios (Usuario u[MAX_USUARIOS], int cant) {
    srand(time(0));
    for (int i = 0; i < cant; i++) {
        int n = rand() % 5;

        u[i].codigo = i + 1;
        u[i].nombre = nombresUsuarios[n].c_str() + std::to_string(i);
        // Me canse de randomizar cosas
        u[i].fechaRegistro = 20200625; 
    }
}

void generarCompras (Usuario u[MAX_USUARIOS], int cantU, Juego j[MAX_JUEGOS], int cantJ, Compra c[], int cant) {
    for (int i = 0; i < cant; i++) {
        int n = rand() % cantU;
        int m = rand() % cantJ;

        c[i].usr = u[n];
        c[i].jog = j[m];
    }
}

int main() {

    Juego juegos[10];
    generarJuegos(juegos, 10);

    Usuario usuarios[10];
    generarUsuarios(usuarios, 10);

    Compra compras[10];
    // Si, pueden haber compras repetidas pero bueno, esto no es Apple viejo
    generarCompras(usuarios, 10, juegos, 10, compras, 10);

    ordenarComprasPorJuego(compras, 10);
    ordenarComprasPorUsuario(compras, 10);
    listarUsuariosPorJuegos(compras, 10);

    return 0;
}
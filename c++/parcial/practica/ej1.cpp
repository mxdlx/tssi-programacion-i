#include <iostream>
using namespace std;

int ingresarValor () {
    int r = 0;

    cout << "Ingrese N: ";
    cin >> r;
    cout << endl;

    return r;
}

void llenarArray (int a[], int cant) {
    for (int i = 0; i < cant; i++) {
        cout << "Ingrese un numero: ";
        cin >> a[i];
        cout << endl;
    }
}

void sumaCruzada (int a[], int b[], int s[], int cant) {
    int i = 0;
    while (i < cant) {
        s[i] = a[i] + b[cant-1-i];
        i++;
    }
}

void imprimir (int a[], int cant, int r) {
    // Ya vengo usando printf por todos lados, disculpen
    printf("Resultado %d: \n", r);
    //cout << "Resultado 1: " << endl;
    for (int h = 0; h < cant; h++) {
        cout << a[h] << endl;
    }
}

void restaMiembroAMiembro (int a[], int b[], int s[], int cant) {
    int j = 0;
    while (j < cant) {
        s[j] = a[j] - b[j];
        j++;
    }
}

void productoEscalar (int a[], int b[], int s[], int cant) {
    for (int k = 0; k < cant; k++)
    {
        s[k] = s[k] + (a[k] * b[k]);
    }
}

int main () {
    //int n = 0;
    //int m = 0;

    //cout << "Ingrese N: ";
    //cin >> n;
    //cout << endl;
    //int v1[n];

    //cout << "Ingrese N: ";
    //cin >> m;
    //cout << endl;
    //int v2[m];

    //for (int i = 0; i < n; i++)
    //{
    //    cout << "Ingrese un numero: ";
    //    cin >> v1[i];
    //    cout << endl;
    //}
    //
    //for (int i = 0; i < n; i++)
    //{
    //    cout << "Ingrese un numero: ";
    //    cin >> v2[i];
    //    cout << endl;
    //}

    //int i = 0;
    //while (i < m)
    //{
    //    s[i] = v1[i] + v2[m-1-i];
    //    i++;
    //}

    //cout << "Resultado 1: " << endl;
    //for (int h = 0; h < n; h++)
    //{
    //    cout << s[h] << endl;
    //}

    //int j = 0;
    //while (j < m)
    //{
    //    d[j] = v1[j] - v2[j];
    //    j++;
    //}

    //cout << "Resultado 2: " << endl;
    //for (int h = 0; h < n; h++)
    //{
    //    cout << d[h] << endl;
    //}

    //for (int k = 0; k < n; k++)
    //{
    //    p[k] = p[k] + (v1[k] * v2[k]);
    //}

    int n = ingresarValor();
    int v1[n];

    int m = ingresarValor();
    int v2[m];

    if (n != m){
        // termina el programa
        return 1;
    }

    llenarArray(v1, n);
    llenarArray(v2, m);

    int s[n];
    int d[n];

    sumaCruzada(v1, v2, s, n);
    imprimir(s, n, 1);

    restaMiembroAMiembro(v1, v2, d, n);
    imprimir(d, n, 2);

    int p[n] = {0};
    productoEscalar(v1, v2, p, n);
    
    cout << "Resultado del producto escalar: " << endl;
    for (int h = 0; h < n; h++)
    {
        cout << p[h] << endl;
    }

    // RESPUESTA PUNTO 2:
    // El vector p necesita ser inicializado ya que es utilizado en el mismo calculo que se usa para rellenarlo.
    // Si no estuviera inicializado tendriamos basura en sus posiciones.

    return 0;
}
#include <iostream>

using namespace std;
/*

Completar las funciones faltantes para que el siguiente programa realice lo siguiente:

Dados dos vectores de numeros enteros, sin repetidos y ordenados en forma ascendente obtener e imprimir por consola los valores de un
tercer vector con los elementos que están en ambos conjuntos en forma ascendente

ej:

A={-12, -8, -2, 0, 1, 5, 8, 10}    y B={-7, -2, 1, 5, 9, 11}   entonces c={-2, 1, 5}


*/

// Esta funcion debe recibir dos vectores y completar un tercero con los elementos comunes a ambos sin repetidos y en forma ascendente.
void entregado(int a[], int cantA, int b[], int cantB, int c[], int& cantC) {
    int k = 0;

    if (cantA < cantB) {
        for (int i = 0; i < cantA; i++) {
            for (int j = 0; j < cantB; j++) {
                if (a[i] == b[j]) {
                    printf("%d", a[i]);
                    c[k] = a[i];
                    k++;
                }
            }
        }
    } else {
        for (int i = 0; i < cantB; i++) {
            for (int j = 0; j < cantA; j++) {
                if (b[i] == a[j]) {
                    printf("%d", b[i]);
                    c[k] = b[i];
                    k++;
                }
            }
        }
    }
}

void apareo(int a[], int cantA, int b[], int cantB, int c[], int& cantC) {
    if (cantA < cantB) {
        for (int i = 0; i < cantA; i++) {
            for (int j = 0; j < cantB; j++) {
                if (a[i] == b[j]) {
                    c[cantC] = a[i];
                    cantC++;
                }
            }
        }
    } else {
        for (int i = 0; i < cantB; i++) {
            for (int j = 0; j < cantA; j++) {
                if (b[i] == a[j]) {
                    c[cantC] = b[i];
                    cantC++;
                }
            }
        }
    }    
}

void imprimir(int vec[], int cant)
{
    cout << "Copiar los valores entre numerales #99999999#" << endl
         << "=========================================="
         << endl
         << "#";
    for (int i = 0; i < cant; i++)
    {
        cout << vec[i] << "";
    }
    cout << "#"
         << endl
         << "==========================================" << endl;
}

int main()
{
    int a[] = {-12,-8,-2,0,1,5,8,10};
    int b[] = {-7,-2,1,5,9,11};
    int c[128];

    int bigA[] = {-48,-47,-46,-43,-37,-36,-34,-32,-31,-30,
                  -29,-27,-26,-25,-24,-23,-22,-21,-20,-19,
                  -18,-17,-16,-13,-12,-11,-10,-9,-8,-7,
                  -5,-4,-3,-2,1,3,4,5,7,10,
                  11,15,22,23,25,26,26,27,28,30,
                  33,34,35,37,38,39,40,41,42,43,
                  44,45,46};

    int bigB[] = {-50,-49,-45,-44,-43,-42,-41,-40,-39,-38,
                  -36,-35,-34,-33,-31,-30,-29,-26,-25,-24,
                  -23,-22,-20,-18,-17,-16,-14,-13,-12,-11,
                  -9,-8,-6,-5,-3,0,3,4,6,7,
                  8,10,11,12,14,15,21,22,23,24,
                  25,26,27,29,30,32,35,36,37,38,
                  41,42,46,47,49};

    int bigC[128];

    int cant = 0;
    //entregado(vecA, 8, vecB, 6, vecC, cant);
    //apareo(a, 8, b, 6, c, cant);

    apareo(bigA, 63, bigB, 65, bigC, cant);
    imprimir(bigC, cant);
}
#include <string>
using namespace std;

struct Alumno {
    int legajo;
    string nombre;
};

struct Examen {
    Alumno alum;
    int nota;
};
#include <iostream>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

// Helpers
bool checkArray (int first[], int segundo[], int cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        //printf("%d - %d\n", first[i], segundo[i]);
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

// 5x4
int fixture[MAX_FILAS][MAX_COLUMNAS] = {{0,1,2,3},{4,5,6,7},{8,9,10,11},{12,13,14,15},{16,17,18,19}};

TEST_CASE ( "Printing test" ) {
    string esperado = "[  0  1  2  3  ]\n[  4  5  6  7  ]\n[  8  9  10  11  ]\n[  12  13  14  15  ]\n[  16  17  18  19  ]\n";
    string r = printMatriz(fixture, 5, 4);
    
    CHECK_THAT( esperado, Catch::Equals(r) );
}

TEST_CASE ( "Promedio" ) {
    float esperado = 9.5;
    float r = promedio(fixture, 5, 4);

    REQUIRE( esperado == r );
}

TEST_CASE ( "Maximo" ) {
    int esperado_max = 19;
    int esperado_x = 5;
    int esperado_y = 4;
    int r_max = 0;
    int r_x = 0;
    int r_y = 0;

    mayor(fixture, 5, 4, r_max, r_x, r_y);

    REQUIRE( esperado_max == r_max );
    REQUIRE( esperado_x == r_x );
    REQUIRE( esperado_y == r_y );
}

TEST_CASE ( "Suma de columnas" ) {
    int esperado[4] = {40, 45, 50, 55};
    int r[4];

    sumcolum(fixture, 5, 4, r);

    bool equalArrays = checkArray(esperado, r, 4);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Maximo fila" ) {
    int esperado[5] = {3, 7, 11, 15, 19};
    int r[5];

    maxfila(fixture, 5, 4, r);

    bool equalArrays = checkArray(esperado, r, 4);

    REQUIRE( equalArrays );
}
#include <iostream>
#include <vector>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

bool checkArray (int first[], int segundo[], int cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        //printf("%d - %d\n", first[i], segundo[i]);
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

int cantElementosTriangular (int rows) {
    int contador = 0;

    for (int i = (rows - 1); i > 0; i--) {
        contador += i;
    }

    return contador;
}

int fixtureCuadrada[MAX_COLUMNAS][MAX_COLUMNAS] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
int fixtureNoCuad[MAX_FILAS][MAX_COLUMNAS] = {{0, 1}, {2, 3}, {4, 5}};
int fixtureCuadradaPar[MAX_COLUMNAS][MAX_COLUMNAS] = {{10, 10, 9, 9}, {10, 10, 9, 9}, {8, 8, 7, 7}, {7, 7, 6, 6}};

TEST_CASE ( "Imprimir solo primera columna" ) {
    int esperado[3] = {0, 3, 6};
    int r[3];

    obtenerColumna(fixtureCuadrada, 3, 3, 0, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );    
}

TEST_CASE ( "Imprimir solo segunda columna" ) {
    int esperado[3] = {1, 4, 7};
    int r[3];

    obtenerColumna(fixtureCuadrada, 3, 3, 1, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );    
}

TEST_CASE ( "Imprimir solo tercera columna" ) {
    int esperado[3] = {2, 5, 8};
    int r[3];

    obtenerColumna(fixtureCuadrada, 3, 3, 2, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );    
}

TEST_CASE ( "Promedio de la matriz" ) {
    float esperado = 4;
    float r = promedio(fixtureCuadrada, 3, 3);

    REQUIRE( esperado == r );
}

TEST_CASE ( "Array con suma de columnas - Cuadrada" ) {
    int esperado[3] = {9, 12, 15};
    int r[3];

    genSumaColumna(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Array con suma de columnas - No cuadrada" ) {
    int esperado[2] = {6, 9};
    int r[2];

    genSumaColumna(fixtureNoCuad, 3, 2, r);

    bool equalArrays = checkArray(esperado, r, 2);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Array con maximo de cada fila" ) {
    int esperado[3] = {2, 5, 8};
    int r[3];

    maxCadaFila(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Diagonal principal" ) {
    int esperado[3] = {0, 4, 8};
    int r[3];

    obtenerDiagonalPal(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Diagonal NO principal" ) {
    int esperado[3] = {2, 4, 6};
    int r[3];

    obtenerDiagonalNoPal(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Sumatoria diagonal principal" ) {

    REQUIRE( 12 == sumatoriaDiagonalPal(fixtureCuadrada, 3, 3) );
}

TEST_CASE ( "Sumatoria diagonal NO principal" ) {

    REQUIRE( 12 == sumatoriaDiagonalNoPal(fixtureCuadrada, 3, 3) );
}

TEST_CASE ( "Mayor cuarto cuadrada par" ) {
    int esperado[4] = {10, 10, 10, 10};
    int r[4];

    maxCuarto(fixtureCuadradaPar, 4, 4, r);

    bool equalArrays = checkArray(esperado, r, 4);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Triangular superior ") {
    int cantElementos = cantElementosTriangular(3);
    int esperado[cantElementos] = {1, 2, 5};
    int r[cantElementos];

    obtenerTriangularSup(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Triangular inferior ") {
    int cantElementos = cantElementosTriangular(3);
    int esperado[cantElementos] = {3, 6, 7};
    int r[cantElementos];

    obtenerTriangularInf(fixtureCuadrada, 3, 3, r);

    bool equalArrays = checkArray(esperado, r, 3);

    REQUIRE( equalArrays );
}
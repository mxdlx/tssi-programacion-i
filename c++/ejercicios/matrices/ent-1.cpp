#include <iostream>
#include "funciones.h"
using namespace std;

#define MAX_FILAS 30
#define MAX_COLUMNAS 25

/* 17. Ingresar dos valores, M (< 30) y N (< 25) y a continuación por filas todos los componentes de una matriz MATRIZA de M filas y N columnas.

Desarrollar un programa que:
a) Imprima la matriz MATRIZA por columnas.
b) Calcule e imprima el valor promedio de los componentes de la matriz.
c) Genere e imprima un vector VECSUMCOL donde cada componente sea la suma de la columna homóloga.
d) Genere e imprima un vector VECMAXFIL donde cada componente sea el valor máximo de cada fila. */

int main() {
    int matriz[MAX_FILAS][MAX_COLUMNAS];
    int rows = 0;
    int cols = 0;

    printf("Ingrese el numero de filas: ");
    scanf("%d", &rows);

    printf("Ingrese el numero de columnas: ");
    scanf("%d", &cols);

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("Ingrese el valor de la posicion [%d][%d]: ", i, j);
            scanf("%d", &matriz[i][j]);
        }
    }

    printf("\n>> Mostrando la matriz columna por columna: \n");
    for (int i = 0; i < cols; i++) {
        int r[cols];

        obtenerColumna(matriz, rows, cols, i, r);

        printf("Columna %d: ", i);
        for (int j = 0; j < rows; j++) {
            printf("%d ", r[j]);
        }
        printf("\n");
    }

    printf("\n>> El promedio de los componentes de la matriz es: %4.2f\n", promedio(matriz, rows, cols));

    printf("\n>> Ejercicio C, un vector cuyos componentes son la suma de la columna homóloga: \n");
    {
        int r[cols];
        genSumaColumna(matriz, rows, cols, r);

        printf("El vector es: [ ");
        for (int i = 0; i < cols; i++) {
            printf("%d ", r[i]);
        }
        printf("]\n");
    }
    
    printf("\n>> Ejercicio D, un vector cuyos componentes son el maximo de cada fila: \n");
    {
        int r[rows];
        maxCadaFila(matriz, rows, cols, r);

        printf("El vector es: [ ");
        for (int i = 0; i < rows; i++) {
            printf("%d ", r[i]);
        }
        printf("]\n");
    }

    
    return 0;
}
#include <iostream>
#include <vector>
using namespace std;

char posNeg(int valor) {
    // Asumimos que nunca nos van a pedir un 0
    if (valor > 0) {
        return 'P';
    } else if (valor < 0) {
        return 'N';
    }
}

float celsiusAFahrenheit(float temperatura) {
    return 9.0/5.0 * temperatura + 32;
}

bool entreCeroYNueve(int valor) {
    return valor >= 0 && valor <= 9;
}

bool esVocal(char letra) {
    return letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' ||\
    letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U';
}

int mayor(int numeroA, int numeroB) {
    if (numeroA > numeroB) {
        return numeroA;
    } else {
        return numeroB;
    }
}

void nPrimos(vector<int>& primos, int cantidad) {
    int contador = 0;
    int numero = 2;

    while (contador < cantidad) {
        bool esPrimo = true;

        for(int i = (numero - 1); i > 1; i--) {
            if (numero % i == 0) {
                esPrimo = false;
            }
        }

        if (esPrimo) {
            primos.push_back(numero);
            contador++;
            numero++;
        } else {
            numero++;
        }
    }
}
#include <iostream>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

bool checkArray (int first[], int segundo[], int cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        //printf("%d - %d\n", first[i], segundo[i]);
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

TEST_CASE ( "Test maximos en Array" ) {
    int fixture[5] = {4,4,1,2,3};
    int esperado[5] = {0,1,0,0,0};
    int fillme[5];

    maximosArray(fixture, 5, fillme);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE ( equalArrays );
}

TEST_CASE ( "Extremos" ) {
    int fixtureA[5] = {1,2,3,4,5};
    int fixtureB[5] = {10,20,30,40,50};
    int esperado[5] = {51,42,33,24,15};
    int fillme[5];

    extermos(fixtureA, fixtureB, fillme, 5);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE ( equalArrays );
}

TEST_CASE ( "Pesquisa - OOB Left" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 0.1;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == -1 );
    REQUIRE ( posicionLeft == -1 );
    REQUIRE ( posicionRight == -1 );
    REQUIRE ( mayorMenor == true );
}

TEST_CASE ( "Pesquisa - OOB Right" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 251.3;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == -1 );
    REQUIRE ( posicionLeft == -1 );
    REQUIRE ( posicionRight == -1 );
    REQUIRE ( mayorMenor == true );
}

TEST_CASE ( "Pesquisa - Valor coincidente" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 5.6;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == 3 );
    REQUIRE ( posicionLeft == 2 );
    REQUIRE ( posicionRight == 4 );
    REQUIRE ( mayorMenor == false );
}

TEST_CASE ( "Pesquisa - Primeras posiciones" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 1.1;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == -1 );
    REQUIRE ( posicionLeft == 0 );
    REQUIRE ( posicionRight == 1 );
}

TEST_CASE ( "Pesquisa - Middle" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 4.0;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == -1 );
    REQUIRE ( posicionLeft == 2 );
    REQUIRE ( posicionRight == 3 );
}

TEST_CASE ( "Pesquisa - Antes del fin" ) {
    float fixture[5] = {0.3, 1.2, 3.444, 5.6, 90.123};
    float pesquisa = 25.6;
    int posicionCoincidente = 0;
    int posicionLeft = 0;
    int posicionRight = 0;
    bool mayorMenor = true;

    laPesquisa(fixture, 5, pesquisa, posicionCoincidente, posicionLeft, posicionRight, mayorMenor);

    REQUIRE ( posicionCoincidente == -1 );
    REQUIRE ( posicionLeft == 3 );
    REQUIRE ( posicionRight == 4 );
}

TEST_CASE ( "Palindromo - True Impar" ) {
    char fixture[7] = {'N', 'E', 'U', 'Q', 'U', 'E', 'N'};

    REQUIRE ( palindromo(fixture, 7) == true );
}

TEST_CASE ( "Palindromo - True Par" ) {
    char fixture[4] = {'A', 'N', 'N', 'A'};

    REQUIRE ( palindromo(fixture, 4) == true );
}

TEST_CASE ( "Palindromo - False" ) {
    char fixture[6] = {'C', 'H', 'U', 'B', 'U', 'T'};

    REQUIRE ( palindromo(fixture, 7) == false );
}
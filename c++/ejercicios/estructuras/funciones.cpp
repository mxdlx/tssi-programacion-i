#include "estructuras.h"
#include <iostream>
#include <math.h>
#include <stdio.h>
using namespace std;

int distancia (Punto posFinal, Punto posInicial) {
    return sqrt(pow((posFinal.X - posInicial.X), 2) + pow((posFinal.Y - posInicial.Y), 2));
}

void ecuacionRectaDosPuntos (Punto posFinal, Punto posInicial, float& pendiente, float& ordenada) {
    // Vamos por la pendiente
    pendiente = (posFinal.Y - posInicial.Y) / (posFinal.X - posInicial.X);

    // Vamos por la ordenada
    ordenada = posFinal.Y - pendiente * posFinal.X;
}

void cincoPuntos (Punto posInicial, Punto posFinal, float puntos[5]) {
    float pendiente, ordenada;

    ecuacionRectaDosPuntos(posInicial, posFinal, pendiente, ordenada);

    for (int i = 0; i < 5; i++) {
        puntos[i] = pendiente * i + ordenada;
    }
}

bool stock (Receta& receta) {
    bool r = true;
    for (int i = 0; i < 3; i++) {
        if (receta.ingredientes[i].ing.stock < receta.ingredientes[i].cantidad) {
            r = false;
        }
    }
    return r;
}

void cocinar (Receta& receta) {
    for (int i = 0; i < 3; i++) {
        receta.ingredientes[i].ing.stock -= receta.ingredientes[i].cantidad; 
    }
}

bool stockCritico (Ingrediente& ing) {
    bool r = false;
    float valorCritico = ing.stockMaximo * 0.3;

    if (float(ing.stock) < valorCritico) {
        r = true;
    }

    return r;
} 
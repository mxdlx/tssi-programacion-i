#include <iostream>
using namespace std;

int factorial (int valor) {
    if (valor == 0) {
        return 1;
    } else {
        return valor * factorial(valor - 1);
    }
}

int sumarArray (int r[], int tamanio) {
    int sumador = 0;

    for (int i = 0; i < tamanio; i++) {
        sumador += r[i];
    }

    return sumador;
}

void arrayNaturalesPares (int r[], int tamanio) {
    int contador = 0;
    int indice = 0;

    while (tamanio > 0) {
        if (contador % 2 == 0) {
            r[indice] = contador;
            indice++;
            tamanio--;
        }
        contador++;
    }
}

void ultimoNegativo (int in[], int tamanio, int out[]) {
    // Performance--
    int contador = 0;
    int outindex = 0;

    if (in[tamanio - 1] < 10) {
        for (int i = 0; i < tamanio; i++) {
            if (in[i] < 0) {
                out[outindex] = in[i];
                outindex++;
                contador++;
            }
        }

        for (int i = 0; i < tamanio; i++) {
            if (i >= contador) {
                out[i] = 0;
            } 
        }

    } else {
        for (int i = 0; i < tamanio; i++) {
            if (in[i] >= 0) {
                out[outindex] = in[i];
                outindex++;
                contador++;
            }
        }

        for (int i = 0; i < tamanio; i++) {
            if (i >= contador) {
                out[i] = 0;
            } 
        }
    }
}

void vectFact (int vec[], int fact[], int tamanio) {
    for (int i = 0; i < tamanio; i++) {
        fact[i] = factorial(vec[i]);
    }
}

void indicesSigno (int in[], int out[], int tamanio) {
    int contador = 0;
    int outindex = 0;

    if ( sumarArray(in, tamanio) > 0) {
        for (int i = 0; i < tamanio; i++) {
            if (i % 2 != 0) {
                out[outindex] = in[i];
                outindex++;
                contador++;
            }
        }

        for (int i = 0; i < tamanio; i++) {
            if (i >= contador) {
                out[i] = 0;
            } 
        }
    } else {
       for (int i = 0; i < tamanio; i++) {
            if (i % 2 == 0) {
                out[outindex] = in[i];
                outindex++;
                contador++;
            }
        }

        for (int i = 0; i < tamanio; i++) {
            if (i >= contador) {
                out[i] = 0;
            } 
        } 
    }
}

void intercalador(int a[], int b[], int tamanio, int r[]) {
    for (int i = 0; i < tamanio; i++) {
        if (i % 2 == 0) {
            r[i] = a[i];
        } else {
            r[i] = b[i];
        }
    }
}
#include <iostream>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

bool checkArray (int first[], int segundo[], int cantidad) {
    bool equalArrays = true;

    for (int i = 0; i < cantidad; i++) {
        if (first[i] != segundo[i]) {
            equalArrays = false;
        }
    }

    return equalArrays;
}

TEST_CASE ( "Array Naturales Pares" ) {
    int fixture[5] = {0,2,4,6,8};
    int fillme[5];

    arrayNaturalesPares(fillme, 5);

    bool equalArrays = checkArray(fixture, fillme, 5);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Ultimo negativo - Primer caso" ) {
    int fixture[5] = {-1,5,-4,2,10};
    int esperado[5] = {5,2,10,0,0};
    int fillme[5];

    ultimoNegativo(fixture, 5, fillme);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Ultimo negativo - Segundo caso" ) {
    int fixture[5] = {-1,5,-4,2,8};
    int esperado[5] = {-1,-4,0,0,0};
    int fillme[5];

    ultimoNegativo(fixture, 5, fillme);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE( equalArrays );
}

TEST_CASE ( "ALL HAIL VECT FACT" ) {
    int fixture[4] = {5,6,3,4};
    int esperado[4] = {120,720,6,24};
    int fillme[4];

    vectFact(fixture, fillme, 4);

    bool equalArrays = checkArray(esperado, fillme, 4);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Imprimir los valores con indice impar si la suma del array es positiva" ) {
    int fixture[5] = {1,-3,5,10,-9};
    int esperado[5] = {-3,10,0,0,0};
    int fillme[5];

    indicesSigno(fixture, fillme, 5);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Imprimir los valores con indice par si la suma del array es menor o igual a cero" ) {
    int fixture[6] = {1,-12,-3,5,-10,-9};
    int esperado[6] = {1,-3,-10};
    int fillme[6];

    indicesSigno(fixture, fillme, 6);

    bool equalArrays = checkArray(esperado, fillme, 6);

    REQUIRE( equalArrays );
}

TEST_CASE ( "Intercalador" ) {
    int primero[5] = {2,13,65,71,91};
    int segundo[5] = {8,9,10,6,-12};
    int esperado[5] = {2,9,65,6,91};
    int fillme[5];

    intercalador(primero, segundo, 5, fillme);

    bool equalArrays = checkArray(esperado, fillme, 5);

    REQUIRE( equalArrays );
}
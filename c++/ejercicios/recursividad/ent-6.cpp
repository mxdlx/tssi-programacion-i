#include <iostream> 
using namespace std;

int factorial(int n){
    if(n==0){ 
        return 1;
    }

    // La linea original hace un pre decremento de la variable,
    // esto hace que se evalue la expresion con el valor de n reducido en 1,
    // por lo tanto cuando n es 1, la linea es compilada como:
    // return 0 * factorial(0);
    // Lo cual anula el resultado
    //return n * factorial(--n); 

    // Linea correcta a continuacion
    return n * factorial(n - 1);
}

int main () {
    cout << factorial(4) << endl; return 0;
}
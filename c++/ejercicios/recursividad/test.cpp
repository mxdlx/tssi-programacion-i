#include <iostream>
#include <vector>
#include "../../test/catch.hpp"
#include "funciones.h"
using namespace std;

TEST_CASE( "Serie L para 5" ) {
    REQUIRE( serieL(5) == 15 );
}

TEST_CASE( "Serie E" ) {
    REQUIRE( seriee(5) > 2.6 );
    REQUIRE( seriee(5) < 2.8 );
}

TEST_CASE( "Collatz para 18" ) {
    vector<int> pv{18,9,28,14,7,22,11,34,17,52,26,13,40,20,10,5,16,8,4,2,1};
    vector<int> tv;

    collatz(18, tv);

    CHECK_THAT( tv, Catch::Equals(pv) );
}

TEST_CASE( "Fibonacci para 10" ) {
    REQUIRE( fibonacci(10) == 55 );
}

TEST_CASE( "Octal" ) {
    REQUIRE( decimalToOctal(372) == 564 );
}
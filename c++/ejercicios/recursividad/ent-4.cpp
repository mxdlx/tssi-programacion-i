#include <iostream>
#include "funciones.h"
using namespace std;

/* En matemáticas, la sucesión o serie de Fibonacci es la siguiente sucesión infinita de números naturales:
0 , 1 , 1 , 2 , 3 , 5 , 8 , 13 , 21 , 34 , 55 , ...
La sucesión comienza con los números 0 y 1, a partir de estos, cada término es la suma de los dos anteriores
Desarrolle una función recursiva que permita obtener la serie de Fibonacci a un determinado término.
Ejemplo: Para N = 5, la serie de Fibonacci será 0 , 1 , 1 , 2 , 3 */

int main() {
    int valor = -1;

    while (valor < 0) {
        printf("Ingrese un valor para la serie de Fibonacci: ");
        scanf("%d", &valor);
    }

    printf("El valor de la serie de Fibonacci para %d es: %d\n", valor, fibonacci(valor));

    return 0;
}
#include <iostream>
#include "funciones.h"
using namespace std;

/* La constante matemática “e” puede aproximarse según la siguiente serie numérica:
e = 1 + 1/1! + 1/2! + 1/3! + .... + 1/n! = 2,718...
Desarrollar una función recursiva que permita obtener el valor aproximado del número “e” según una cantidad de términos
determinada por el usuario.
Nota: puede desarrollar funciones auxiliares si lo necesita. */

int main() {
    // Tengo que revisar la cantidad de decimales de tipo float pero tengo que seguir con los siguientes ejercicios
    // Con valores altos explota por overflow
    int valor = 0;

    while (valor < 1) {
        printf("Ingrese un valor para la serie E: ");
        scanf("%d", &valor);
    }
    
    printf("El resultado es %4.10f\n", seriee(valor));
    return 0;
}
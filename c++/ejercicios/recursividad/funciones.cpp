#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

int serieL(int cantidad) {
    if (cantidad == 0) {
        return 0;
    } else {
        return cantidad + serieL(cantidad - 1);
    }
}

int factorial (int valor) {
    if (valor == 0) {
        return 1;
    } else {
        return valor * factorial(valor - 1);
    }
}

float seriee (int valor) {
    if (valor == 0) {
        return 1;
    } else {
        return (1.0/factorial(valor)) + seriee(valor - 1);
    }
}

void collatz(int valor, vector<int>& r) {
    if (valor == 1) {
        r.push_back(valor);
    } else if (valor % 2 == 0) {
        r.push_back(valor);
        collatz(valor / 2, r);
    } else {
        r.push_back(valor);
        collatz(valor * 3 + 1, r);
    }
}

int fibonacci (int valor) {
    if (valor < 2) {
        return valor;
    } else {
        return fibonacci(valor - 1) + fibonacci(valor - 2);
    }
}

void vectorOctal(int valor, vector<int>& r) {
    if (valor < 8) {
        r.push_back(valor);
    } else {
        int cociente = valor / 8;
        int resto = valor % 8;

        r.push_back(resto);
        vectorOctal(cociente, r);
    }
}

int decimalToOctal(int valor) {
    std::vector<int> r;
    int sumador = 0;

    vectorOctal(valor, r);

    for (int i = 0; i < r.size(); i++) {
        sumador += pow(10, i) * r.at(i); 
    }

    return sumador;
}
#include <string>
using namespace std;

struct FechaNacimiento {
    int dia;
    int mes;
    int anio;
};

struct Persona {
    string nombre;
    FechaNacimiento fecNac;
};

struct Boleta {
    string apellido;
    int legajo;
    int codigo_materia;
    int dia;
    int mes;
    int anio;
};
#include <iostream>
#include "../../test/catch.hpp"
#include "estructuras.h"
#include "funciones.h"
using namespace std;

// Fixtures
Persona mayor = {"Alvaro", {26,8,1989}};
Persona menor = {"Maria", {22,12,1990}};

Persona mismo_anio_mayor = {"Jose", {21,1,1990}};
Persona mismo_anio_menor = {"Carla", {22,2,1990}};

Persona mismo_anio_mes_mayor = {"Alvaro", {21,1,1990}};
Persona mismo_anio_mes_menor = {"Maria", {22,1,1990}};

Persona mismo_a = {"Alvaro", {21,1,1990}};
Persona mismo_b = {"Maria", {21,1,1990}};

Persona personas[4] = {
    mayor,
    menor,
    mismo_anio_mayor,
    mismo_anio_menor,
};

TEST_CASE ( "esMayor para True" ) {
    REQUIRE(esMayor(mayor, menor) == true);    
}

TEST_CASE ( "esMayor para False" ) {
    REQUIRE(esMayor(menor, mayor) == false);
}

TEST_CASE ( "esMayor mismo anio para True" ) {
    REQUIRE(esMayor(mismo_anio_mayor, mismo_anio_menor) == true);    
}

TEST_CASE ( "esMayor mismo anio para False" ) {
    REQUIRE(esMayor(mismo_anio_menor, mismo_anio_mayor) == false);    
}

TEST_CASE ( "esMayor mismo anio, mismo mes para True" ) {
    REQUIRE(esMayor(mismo_anio_mes_mayor, mismo_anio_mes_menor) == true);    
}

TEST_CASE ( "esMayor mismo anio, mismo mes para False" ) {
    REQUIRE(esMayor(mismo_anio_mes_menor, mismo_anio_mes_mayor) == false);    
}

TEST_CASE ( "esMayor mismo todo" ) {
    REQUIRE(esMayor(mismo_a, mismo_b) == false);    
}

TEST_CASE ( "Ordenar por fecha de nacimiento" ) {
    ordenarPorFechaNacimiento(personas, 4);

    REQUIRE(personas[3].nombre == "Alvaro");
}

//TEST_CASE ( "Generar boletas" ) {
//    int cantidad = 5;
//    Boleta boletas[cantidad];
//
//    generarBoletas(boletas, cantidad);
//
//    for (int i = 0; i < cantidad; i++) {
//        printf("Dia del examen: %d\n", boletas[i].dia);
//        printf("Mes del examen: %d\n", boletas[i].mes);
//        printf("Anio del examen: %d\n", boletas[i].anio);
//        printf("Codigo de la materia: %d\n", boletas[i].codigo_materia);
//        printf("Legajo del alumno: %d\n", boletas[i].legajo);
//        printf("Apellido del alumno: %s\n", boletas[i].apellido.c_str());
//    }
//}

TEST_CASE ( "Registros por codigo de materia para Mayo" ) {
    int cantidad = 10;
    Boleta boletas[cantidad];

    generarBoletasMayo(boletas, cantidad);
    imprimirInscripcionPorMateria(boletas, 10, 874175);
}

TEST_CASE ( "Registros por codigo de materia anteriores a Mayo" ) {
    int cantidad = 10;
    Boleta boletas[cantidad];

    generarBoletasPreviasMayo(boletas, cantidad);
    imprimirInscripcionPorMateria(boletas, 10, 874175);
}

TEST_CASE ( "Anexar boletas anteriores y de Mayo" ) {
    int cantPrevias = 10;
    Boleta previas[cantPrevias];

    int cantMayo = 20;
    Boleta mayo[cantMayo];

    generarBoletasPreviasMayo(previas, cantPrevias);
    generarBoletasMayo(mayo, cantMayo);

    int k = cantPrevias + cantMayo;
    Boleta total[k];

    anexarBoletas(previas, cantPrevias, mayo, cantMayo, total);

    for (int i = 0; i < k; i++) {
        printf("%d >> %d/%d/%d - %s - Leg: %d - Mat: %d\n", i, total[i].dia, total[i].mes, total[i].anio, total[i].apellido.c_str(), total[i].legajo, total[i].codigo_materia);
    }

    ordenarBoletasPorLegajo(total, k);
    for (int i = 0; i < k; i++) {
        printf("%d >> %d/%d/%d - %s - Leg: %d - Mat: %d\n", i, total[i].dia, total[i].mes, total[i].anio, total[i].apellido.c_str(), total[i].legajo, total[i].codigo_materia);
    }

    listarConDosExamenes(total, k);
}
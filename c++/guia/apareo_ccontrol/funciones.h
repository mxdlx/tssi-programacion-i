bool esMayor (Persona &a, Persona &b);
void ordenarPorFechaNacimiento (Persona p[], int cantidad);
void generarBoletasMayo (Boleta boletas[], int cantidad);
void generarBoletasPreviasMayo (Boleta boletas[], int cantidad);
void generarBoletas (Boleta boletas[], int cantidad);
void imprimirInscripcionPorMateria (Boleta b[], int cantidad, int codigo_materia);
void anexarBoletas(Boleta a[], int cantA, Boleta b[], int cantB, Boleta r[]);
void ordenarBoletasPorLegajo (Boleta a[], int cantidad);
void listarConDosExamenes (Boleta a[], int cantidad);
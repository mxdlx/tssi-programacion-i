#include <iostream>
#include <stdlib.h>
#include <ctime>
using namespace std;

/* Dados 50 números enteros, informar el promedio de los mayores que 100 y la suma de los menores que –10. */


int main() {
    float sumaPositivos = 0;
    int sumaNegativos = 0;

    srand(time(0));

    for (int i = 0; i < 50; i++) {
        int coin = (rand() % 2);
        int numero = (rand() % 1000);

        if (coin != 0) {
            numero *= -1;
        }

        if (numero < -10) {
            sumaNegativos += numero;
        } else if (numero > 100) {
            sumaPositivos += numero;
        } else {
            continue;
        }
    }

    printf("El promedio de los numeros mayores que 100 es: %.2f.\n", sumaPositivos / 50);
    printf("La suma de los numeros menores que -10 es: %d.\n", sumaNegativos);

    return 0;
}
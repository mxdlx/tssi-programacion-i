#include <iostream>
using namespace std;

/* Dado un conjunto de valores, que finaliza con un valor nulo, determinar e imprimir (si hubo valores):
   a) El valor máximo negativo
   b) El valor mínimo positivo
   c) El valor mínimo dentro del rango -17.3 y 26.9
   d) El promedio de todos los valores​ . */


int main() {
    float valor = 1.0;
    float mayorNeg = 0.0, minPos = 0.0, minCustom = 0.0;
    float sumador = 0.0;

    int contador = 0;

    while (valor != 0.0) {
        cout << "Ingrese un valor: ";
        cin >> valor;
        sumador += valor;

        if (contador == 0) {
            // Inicializar valores cuando corresponde a sus rangos
            if (valor > 0) {
                minPos = valor;
            } else {
                mayorNeg = valor;
            }

            if (valor > -17.3 && valor < 26.9) {
                minCustom = valor;
            }
        } else {
            if (valor < 0) {
                // Mayor negativo
                if (mayorNeg != 0.0) {
                    // Cuando mayorNeg ya tiene un valor
                    if (valor > mayorNeg) { mayorNeg = valor; } 
                } else {
                    // Cuando mayorNeg todavia no tiene un valor
                    mayorNeg = valor;
                }
            } else if (valor > 0) {
                if (minPos != 0.0) {
                    // Cuando minPos ya tiene un valor
                    if (valor < minPos) { minPos = valor; } 
                } else {
                    minPos = valor;
                }
            }

            if (valor > -17.3 && valor < 26.9) {
                if (minCustom != 0.0) {
                    if (valor < minCustom) { minCustom = valor; }
                } else {
                    minCustom = valor;
                }
            }
        }

        contador += 1;
    }

    printf("El mayor negativo es %4.2f \n", mayorNeg);
    printf("El menor positivo es %4.2f \n", minPos);
    printf("El menor custom es: %4.2f\n", minCustom);
    printf("El promedio es: %4.2f\n", sumador / (contador - 1));

    return 0;
}
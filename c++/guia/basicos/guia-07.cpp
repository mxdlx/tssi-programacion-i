#include <iostream>
using namespace std;

/* Dado tres valores determinar e imprimir una leyenda según sea: “Forman triangulo” o “No forman triángulo”. */


int main() {
    int a, b, c;

    cout << "Ingrese el primer lado: ";
    cin >> a;

    cout << "Ingrese el segundo lado: ";
    cin >> b;

    cout << "Ingrese el tercer lado: ";
    cin >> c;

    if (a + b > c && b + c > a && a + c > b) {
        printf("Es triangulo.\n");
    } else {
        printf("No es triangulo.\n");
    }

    return 0;
}
#include <iostream>
using namespace std;

/* Dado un conjunto de Nombres y Fechas de nacimientos (AAAAMMDD), que finaliza con un 
   Nombre = ‘FIN’, informar el nombre de la persona con mayor edad y el de la más joven. */

/* Funcion mayorMenorIgual() 
Devuelve:
- True si la primera edad es mayor que la segunda
- False en cualquier otro caso
*/
bool esMayor(int edadA, int edadB) {
    int anioA, anioB, mesA, mesB, diaA, diaB;
    anioA = anioB = mesA = mesB = diaA = diaB = 0;

    anioA = edadA / 10000;
    anioB = edadB / 10000;

    if (anioA < anioB) {
        return true;
    } else if (anioA == anioB) {
        mesA = (edadA - anioA * 10000) / 100;
        mesB = (edadB - anioB * 10000) / 100;

        if (mesA < mesB) {
            return true;
        } else if (mesA == mesB) {
            diaA = (edadA - anioA * 10000) - (mesA * 100);
            diaB = (edadB - anioB * 10000) - (mesB * 100);

            if (diaA < diaB) {
                return true;
            } else if (diaA == diaB) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}


int main() {
    string mayor = "", menor = "", nombre = "";
    int fechaMayor = 0, fechaMenor = 0;

    int contador = 0;

    while (true) {
        int fecha = 0;

        cout << "Ingrese el nombre de la persona: ";
        cin >> nombre;

        if (nombre == "FIN") {
            break;
        }

        cout << "Ingrese la fecha de nacimiento de la persona (AAAAMMDD): ";
        cin >> fecha;

        if (contador == 0) {
            fechaMenor = fechaMayor = fecha;
            mayor = menor = nombre;
        } else {
            if (esMayor(fecha, fechaMayor)) {
                fechaMayor = fecha;
                mayor = nombre;
            }

            if (esMayor(fechaMenor, fecha)) {
                fechaMenor = fecha;
                menor = nombre;
            }
        }
        
        contador += 1;
    }

    printf("El nombre de la persona mayor es: %s\n", mayor.c_str());
    printf("El nombre de la persona menor es: %s\n", menor.c_str());
    return 0;
}
#include <iostream>
using namespace std;

/* Se ingresa un conjunto de valores float, cada uno de los cuales representan el sueldo de un empleado,
   excepto el último valor que es cero e indica el fin del conjunto. 
   Se pide desarrollar un programa que determine e informe:
     a) Cuántos empleados ganan menos $1.520.
     b) Cuántos ganan $1.520 o más pero menos de $2.780.
     c) Cuántos ganan $2.780 o más pero menos de $5.999.
     d) Cuántos ganan $5.999 o más. */

int main() {
    int salariosD = 0, salariosC = 0, salariosB = 0, salariosA = 0;
    float muestra = 1;

    while (muestra > 0) {
        cout << "Ingrese el sueldo: ";
        cin >> muestra;

        if (muestra >= 5999) {
            salariosA += 1;
        } else if (muestra >= 2780) {
            salariosB += 1;
        } else if (muestra >= 1520) {
            salariosC += 1;
        } else if (muestra > 0) {
            salariosD += 1;
        }
    }

    printf("La cantidad de empleados que ganan $5999 o mas es: %d\n", salariosA);
    printf("La cantidad de empleados que ganan $2780 o menos que $5999 es: %d\n", salariosB);
    printf("La cantidad de empleados que ganan $1520 o menos que $2780 es: %d\n", salariosC);
    printf("La cantidad de empleados que ganan menos que $1520 es: %d\n", salariosD);

    return 0;
}
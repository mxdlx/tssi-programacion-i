#include <iostream>
using namespace std;

/* Dado un triángulo representado por sus lados L1, L2, L3, determinar e imprimir una leyenda según sea:
equilátero, isósceles o escaleno. */


int main() {
    int a, b, c;

    cout << "Ingrese el primer lado: ";
    cin >> a;

    cout << "Ingrese el segundo lado: ";
    cin >> b;

    cout << "Ingrese el tercer lado: ";
    cin >> c;

    if (a == b == c) {
        printf("Es equilatero.\n");
    } else if (a == b || b == c || a == c) {
        printf("Es isosceles.\n");
    } else {
        printf("Es escaleno.\n");
    }

    return 0;
}
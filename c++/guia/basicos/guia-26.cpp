#include <iostream>
using namespace std;

/* Un buque de carga traslada 100 contenedores a tres diferentes puertos del país.
Los puertos se identifican con los números 1, 2 y 3.
De cada contenedor que el buque traslade se registran los siguientes datos:
- Identificación del contenedor
- Peso del contenedor en kg
- Puerto de arribo (un valor de 1 a 3).
Se pide calcular e informar:
  1) El peso total que el buque debe trasladar
  2) La identificación del contenedor de mayor peso
  3) La cantidad de contenedores que debe trasladar a cada puerto */


int main() {
    int pesoTotal = 0, maxPeso = 0, maxPesoId = 0; 
    int cantPuertoA = 0, cantPuertoB = 0, cantPuertoC = 0;

    srand(time(0));

    for (int i = 0; i < 100; i++) {
        int idContenedor = (rand() % 100000);
        int pesoContenedor = (rand() % 10000);
        int puertoDestino = (rand() % 3) + 1;

        if (i == 0) {
            maxPesoId = idContenedor;
            maxPeso = pesoContenedor;
        } else {
            if (pesoContenedor > maxPeso) {
                maxPeso = pesoContenedor;
                maxPesoId = idContenedor;
            }
        }

        pesoTotal += pesoContenedor;

        switch (puertoDestino) {
        case 1:
            cantPuertoA += 1;
            break;

        case 2:
            cantPuertoB += 1;
            break;

        case 3:
            cantPuertoC += 1;
            break;
        }

        printf("Registrando container %d con un peso de %d kilos con destino a puerto %d \n", idContenedor, pesoContenedor, puertoDestino);
    }

    printf(">> El peso total que el buque debe trasladar es de %d kilos \n", pesoTotal);
    printf(">> El contenedor mas pesado tiene el identificador %d y un peso de %d kilos \n", maxPesoId, maxPeso);
    printf(">> La cantidad de contenedores con destino al Puerto 1 son: %d \n", cantPuertoA);
    printf(">> La cantidad de contenedores con destino al Puerto 2 son: %d \n", cantPuertoB);
    printf(">> La cantidad de contenedores con destino al Puerto 3 son: %d \n", cantPuertoC);
    return 0;
}
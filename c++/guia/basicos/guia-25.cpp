#include <iostream>
#include <unistd.h>
using namespace std;

/* Dada una serie de M pares {color, número} que corresponden a los tiros de una ruleta. Se pide informar:
     a) cuántas veces salió el número cero y el número anterior a cada cero
     b) cuántas veces seguidas llegó a repetirse el color negro
     c) cuántas veces seguidas llegó a repetirse el mismo número y cuál fue
     d) el mayor número de veces seguidas que salieron alternados el rojo y el negro
     e) el mayor número de veces seguidas que se negó la segunda docena. */

int rojo[18] = {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
int negro[18] = {2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35};

string getColor(int numero) {
    string color = "";

    if (numero == 0) {
        color = "verde";
    } else {
        for (int i = 0; i < 18; i++) {
            if (numero == rojo[i]) {
                color = "rojo";
                i = 18;
            }

            if (numero == negro[i]) {
                color = "negro";
                i = 18;
            }
        }
    }

    return color;
}

int main() {

    int valor = 0;
    int contCero = 0, antCero = 0;
    int contNegro = 0;
    int antSame = -1, contSame = 0;
    int contNoSegDocena = 0, maxNoSegDocena = 0;

    string color = "";

    srand(time(0));

    while (true) {
        valor = (rand() % 37);
        color = getColor(valor);

        printf("El crupier dice: %s el %d!\n", color.c_str(), valor);

        if (valor == 0){
            contCero += 1;

            if (contCero == 1) {
                printf("\n---- Evento ----\n");
                printf("El cero ya salio una vez!\n");
                printf("El numero anterior a cero fue %d!\n", antCero);
            } else {
                printf("\n---- Evento ----\n");
                printf("El cero ya salio %d veces!\n", contCero);
                printf("El numero anterior a cero fue %d!\n", antCero);
            }
            
            printf("\nSeguimos en 5 segundos!\n");
            sleep(5);
        } else {
            antCero = valor;
        }

        if (color == "negro") {
            contNegro += 1;
        } else {
            if (contNegro > 1){
                if (contNegro == 2) {
                    printf("\n---- Evento ----\n");    
                    printf("El color negro llego a repetirse una vez!\n");
                } else {
                    printf("\n---- Evento ----\n");
                    printf("El color negro llego a repetirse %d veces!\n", contNegro);
                }
                
                contNegro = 0;

                printf("\nSeguimos en 5 segundos!\n");
                sleep(5);
            } else {
                contNegro = 0;
            }
        }

        if (valor == antSame) {
            contSame += 1;
        } else {
            if (contSame > 1){
                if (contSame == 2) {
                    printf("\n---- Evento ----\n");    
                    printf("El numero %d llego a repetirse una vez!\n", antSame);
                } else {
                    printf("\n---- Evento ----\n");
                    printf("El numero %d llego a repetirse %d veces!\n", antSame, contSame);
                }

                contSame = 0;

                printf("\nSeguimos en 5 segundos!\n");
                sleep(5);
            } else {
                contSame = 0;
            }
        }

        if (valor < 13 || valor > 24) {
            contNoSegDocena += 1;
        } else {
            if (contNoSegDocena > 1){
                if (contNoSegDocena == 2) {
                    printf("\n---- Evento ----\n");    
                    printf("La segunda docena no salio dos veces seguidas!\n");
                } else {
                    printf("\n---- Evento ----\n");
                    printf("La segunda docena no salio %d veces seguidas!\n", contNoSegDocena);
                }

                if (contNoSegDocena > maxNoSegDocena) {
                    maxNoSegDocena = contNoSegDocena;
                }

                contNoSegDocena = 0;

                printf("El mayor numero de veces seguidas que no salio la segunda docena es: %d", maxNoSegDocena);
                printf("\nSeguimos en 5 segundos!\n");
                sleep(5);
            } else {
                contNoSegDocena = 0;
            }
        }



        antSame = valor;
    }

    return 0;
}
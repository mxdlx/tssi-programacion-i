#include <iostream>
using namespace std;

// ​Dados N valores informar el mayor, el menor y en qué posición del conjunto fueron ingresados.
// Testeado, trust me

int main() {

    int numeroValores = 0;
    int mayor = 0, menor = 0, posMenor = 0, posMayor = 0;

    cout << "Ingrese la cantidad de valores a evaluar: ";
    cin >> numeroValores;

    srand(time(0));

    for (int i = 0; i < numeroValores; i++) {
        int valor = (rand() % 100);

        if (i == 0) {
            menor = valor;
            mayor = valor;
        } else {
            if (valor >= mayor) {
                mayor = valor;
                posMayor = i;
            }

            if (valor <= menor) {
                menor = valor;
                posMenor = i;
            }
        }
    }

    printf("El mayor fue %d en la posicion %d \n", mayor, posMayor);
    printf("El menor fue %d en la posicion %d \n", menor, posMenor);

    return 0;
}
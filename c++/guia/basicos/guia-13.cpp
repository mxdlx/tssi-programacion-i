#include <iostream>
using namespace std;

/* Dados N y M números naturales, informar su producto por sumas sucesivas. */


int main() {
    int a, b, res = 0;

    cout << "Ingrese un numero: ";
    cin >> a;

    cout << "Ingrese otro numero: ";
    cin >> b;

    for (int i = 0; i < b; i++) {
        res += a;
    }

    printf("El resultado de %d * %d es: %d.\n", a, b, res);

    return 0;
}
# Ejercicios de Programación 1 - TSSI - CUVL

Este repositorio incluye ejercicios en C++ de la materia Programación 1 (y su laboratorio) de la Tecnicatura Superior en Sistemas Informáticos de la UTN en el Centro Universitario Vicente López. Cursada 2020, SARS-COV-2.<br>
Durante la cursada me cansé un poco de hacer entrada/salida manual para probar los algoritmos así que comencé a usar unit testing con Catch, sin embargo, no todos los fuentes implementan tests.

## Descripción

Dentro del directorio `c++` hay un conjunto de subdirectorios:

- `algoritmos`: contiene fuentes que muestran los algoritmos básicos que se dan en la cursada.
- `ejercicios`: contiene fuentes de ejercicios presentados semana a semana.
- `guia`: son ejercicios de una guía a la que no le dieron mucha pelota.
- `parcial`: contiene tanto enunciado como fuentes de los parciales teóricos y prácticos de la materia.
- `repaso`: contiene ejercicios de repaso antes del parcial.
- `test`: contiene tanto el fuente de Catch para hacer unit testing como algunas pruebas que hice al principio con ese framework.
- `tp`: contiene ejercicios de los trabajos prácticos de la materia.
